cmake_minimum_required(VERSION 2.8)

project(del_stl)

find_package(VTK REQUIRED)
include(${VTK_USE_FILE})

if(CMAKE_COMPILER_IS_GNUCC OR CMAKE_COMPILER_IS_GNUCXX)
set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++11 -Wall -Wextra -Wno-long-long -pedantic")
endif()

set(DATA_FOLDER "${PROJECT_SOURCE_DIR}/data/")

set(DATA_FOLDER "${PROJECT_SOURCE_DIR}/data" CACHE PATH "Location of externals")
set(RESULTS_FOLDER "${PROJECT_SOURCE_DIR}/results" CACHE PATH "Location of externals")
get_filename_component(DATA_FOLDER ${DATA_FOLDER} REALPATH)
get_filename_component(RESULTS_FOLDER ${RESULTS_FOLDER} REALPATH)

configure_file (
  "${PROJECT_SOURCE_DIR}/pathconfig.h.in"
  "${PROJECT_BINARY_DIR}/pathconfig.h"
  )
include_directories(${PROJECT_BINARY_DIR})
include_directories(${PROJECT_SOURCE_DIR})

#FIND_PACKAGE (Threads)

# c++ 11
set(CMAKE_MODULE_PATH ${CMAKE_MODULE_PATH} "${CMAKE_SOURCE_DIR}/")
include(c11Support.cmake)

find_package(Boost COMPONENTS thread system filesystem program_options REQUIRED)

#add_executable(my_stl main.cpp)
#target_link_libraries(my_stl ${VTK_LIBRARIES})

#add_executable(decmesh decmesh.cpp)
#target_link_libraries(decmesh ${VTK_LIBRARIES})


add_executable(interp_uniform iumesh.cpp)
#target_link_libraries(interp_uniform ${VTK_LIBRARIES} ${CMAKE_THREAD_LIBS_INIT})
target_link_libraries(interp_uniform ${VTK_LIBRARIES} -pthread ${Boost_LIBRARIES} )

add_executable(x3dw x3dwriter.cpp)
target_link_libraries(x3dw ${VTK_LIBRARIES})

add_executable(probeLines probeLines.cpp)
target_link_libraries(probeLines ${VTK_LIBRARIES} -pthread ${Boost_LIBRARIES} )

add_executable(unstructured2structured unstructured2structured.cpp)
target_link_libraries(unstructured2structured ${VTK_LIBRARIES} -pthread ${Boost_LIBRARIES} )

add_executable(excavator excavator.cpp)
target_link_libraries(excavator ${VTK_LIBRARIES} -pthread ${Boost_LIBRARIES} )

add_executable(mapmesh map_mesh.cpp)
target_link_libraries(mapmesh ${VTK_LIBRARIES} -pthread ${Boost_LIBRARIES} )

add_executable(mesh_mainpulator mesh_mainpulator.cpp)
target_link_libraries(mesh_mainpulator ${VTK_LIBRARIES} ${Boost_LIBRARIES} )

add_executable(calcvolume calcvolume.cpp)
target_link_libraries(calcvolume ${VTK_LIBRARIES} ${Boost_LIBRARIES} )

add_executable(write2dm write2dm.cpp)
#target_link_libraries(interp_uniform ${VTK_LIBRARIES} ${CMAKE_THREAD_LIBS_INIT})
target_link_libraries(write2dm ${VTK_LIBRARIES} -pthread ${Boost_LIBRARIES} )

add_executable(extractPolys extractPolys.cpp)
#target_link_libraries(interp_uniform ${VTK_LIBRARIES} ${CMAKE_THREAD_LIBS_INIT})
target_link_libraries(extractPolys ${VTK_LIBRARIES} -pthread ${Boost_LIBRARIES} )


add_executable(clausenClark clausenClark.cpp)
#target_link_libraries(interp_uniform ${VTK_LIBRARIES} ${CMAKE_THREAD_LIBS_INIT})
target_link_libraries(clausenClark ${VTK_LIBRARIES} -pthread ${Boost_LIBRARIES} )


