#include <vtkVersion.h>
#include "2dmReader.h"
#include <vtkPolyData.h>
#include <vtkPointData.h>

#include <vtkTransform.h>
#include <vtkTransformFilter.h>
#include <vtkElevationFilter.h>

#include <vtkXMLPolyDataWriter.h>

#include <vtkStructuredGrid.h>
#include <vtkXMLStructuredGridWriter.h>
#include <vtkXMLStructuredGridReader.h>
#include <vtkXMLPolyDataReader.h>
#include <vtkSimplePointsWriter.h>
#include <vtkSimplePointsReader.h>
#include <vtkCellLocator.h>
#include <vtkDoubleArray.h>
#include "pathconfig.h"
#include <fstream>
#include <iostream>
#include <sstream>
#include <cctype>
#include <chrono>
#include <boost/thread.hpp>

#include "interpolator.h"
using std::ifstream;

#define VTK_CREATE(type, name) \
    vtkSmartPointer<type> name = vtkSmartPointer<type>::New();

template<class writerType>
void writeGeometry( vtkDataObject *geom, const std::string fname )
{
    std::cout << "writing " << fname << "....";
    writerType *writer =  writerType::New();
    writer->SetFileName( fname.c_str() );
#if VTK_MAJOR_VERSION <= 5
    writer->SetInput( geom );
#else
    writer->SetInputData( geom );
#endif
    writer->SetDataModeToBinary();
    writer->Write();
    writer->Delete();
    std::cout << "done." << std::endl;
}

//! \brief type to interpolate
enum InterpolateType
{
    Terrain,
    Manning,
    Monai
};

class regularMesher
{
public:
    regularMesher( const std::string &mshName, double bounds[6], vtkSmartPointer<vtkPolyData> polydata,
                   std::pair<int, int> sizexy, std::pair<double, double> txy, InterpolateType _itype = InterpolateType::Terrain, const int &threadnum = 2 ) :
        probePolyData( polydata ),
        xmin( bounds[0] ), xmax( bounds[1] ),
        ymin( bounds[2] ), ymax( bounds[3] ),
        zmin( bounds[4] ), zmax( bounds[5] ),
        nx( sizexy.first ), ny( sizexy.second ),
        tx( txy.first ), ty( txy.second ),
        numberOfThreads( threadnum ),
        meshName( mshName ),
        itype( _itype )
    { }

    void intepolate_mesh()
    {
        createStructuredGrid();
        std::cout << "performing interpolate mesh..." << std::flush;
        // kreiram atribute od visina poly data
        vtkSmartPointer<vtkElevationFilter> s_elev = vtkSmartPointer<vtkElevationFilter>::New();
        s_elev->SetInputData( probePolyData );
        s_elev->SetHighPoint( 0, 0, zmax );
        s_elev->SetLowPoint( 0, 0, zmin );
        s_elev->SetScalarRange( zmin, zmax );
        s_elev->Update();

        vtkSmartPointer<vtkTransform> translator = vtkSmartPointer<vtkTransform>::New();
        translator->Translate( tx, ty, 0. );
        translator->Update();

        s_flat = vtkSmartPointer<vtkTransformFilter>::New();
        s_flat->SetInputConnection( s_elev->GetOutputPort() );
        s_flat->SetTransform( translator );
        s_flat->Update();

        i_flat = vtkSmartPointer<vtkTransformFilter>::New();
        i_flat->SetInputData( structuredGrid );
        i_flat->SetTransform( translator );
        i_flat->Update();

        writeGeometry<vtkXMLPolyDataWriter> ( s_flat->GetOutput(), filePathBuilder::resultsPath() +
                                              meshName + "_poly_flat_" + std::to_string( nx ) + ".vtp" );
        writeGeometry<vtkXMLStructuredGridWriter> ( i_flat->GetOutput(), filePathBuilder::resultsPath() +
                meshName + "_sgrid_flat_" + std::to_string( nx ) + ".vts" );


        interpolateValues();


        // da izgleda lijepo: stavljam atribute z na mrezu
        vtkSmartPointer<vtkElevationFilter> s_elev2 = vtkSmartPointer<vtkElevationFilter>::New();
        s_elev2->SetInputConnection( i_flat->GetOutputPort() );
        s_elev2->SetHighPoint( 0, 0, zmax );
        s_elev2->SetLowPoint( 0, 0, zmin );
        s_elev2->SetScalarRange( zmin, zmax );
        s_elev2->Update();
        writeGeometry<vtkXMLStructuredGridWriter> ( s_elev2->GetOutput(), filePathBuilder::resultsPath() +
                meshName + "_regular_mesh_" + std::to_string( nx ) + ".vts" );
    }

    void exportToXYZ( const std::string &f_name )
    {
        std::cout << "writing to xyz...";
        std::cout << std::flush;
        vtkSmartPointer<vtkSimplePointsWriter> writer = vtkSmartPointer<vtkSimplePointsWriter>::New();
        writer->SetFileName( f_name.c_str() );
        writer->SetInputConnection( i_flat->GetOutputPort() );
        writer->Write();
        std::cout << "done" << std::endl;
    }


private:
    void interpolateValues()
    {
        std::cout << "interpolating values ..." << std::endl;
        std::chrono::time_point<std::chrono::system_clock> start, end;
        start = std::chrono::system_clock::now();

        vtkDataArray *arrcoords = i_flat->GetOutput()->GetPoints()->GetData();
        std::vector<Interpolator> interpolators;
        std::vector<boost::thread> threads;

        for ( int i = 0; i < numberOfThreads; ++i )
        {
            if ( itype == InterpolateType::Terrain )
            {
                interpolators.push_back( Interpolator( arrcoords, s_flat->GetOutput(),
                                                       []( vtkDataArray * arr, double( & pt ) [3], const int & cellid )
                {
                    if ( ( pt[0] < -790 && pt[1] < 720 ) ||
                            ( pt[0] > 1400 && pt[1] < -290 ) )
                    {
                        pt[2] = -10; // z terena
                        //pt[2] = 0.025; // mng
                        arr->SetTuple( cellid, pt );
                    }
                    else if ( ( pt[0] > 0 && pt[0] < 1400 ) && pt[1] < -153 )
                    {
                        pt[2] = -10; // z terena
                        //pt[2] = 0.025; // mng
                        arr->SetTuple( cellid, pt );
                    }
                    else if ( pt[1] < -153 )
                    {
                        pt[2] = -10; // z terena
                        //pt[2] = 0.025; // mng
                        arr->SetTuple( cellid, pt );
                    }
                    else
                    {
                        pt[2] = 30; // z terena
                        //pt[2] = 0.04; // mng
                        arr->SetTuple( cellid, pt );
                    }
                },
                numberOfThreads ) );
            }
            else if ( itype == InterpolateType::Monai )
            {
                interpolators.push_back( Interpolator( arrcoords, s_flat->GetOutput(),
                                                       []( vtkDataArray * arr, double( & pt ) [3], const int & cellid )
                {
                    if ( pt[1] > 3.402 )
                    {
                        pt[2] = 0.13; // z terena
                        //pt[2] = 0.025; // mng
                        arr->SetTuple( cellid, pt );
                    }
                },
                numberOfThreads ) );
            }
            else // itype == IntepolateType::Manning
            {
                interpolators.push_back( Interpolator( arrcoords, s_flat->GetOutput(),
                                                       []( vtkDataArray * arr, double( & pt ) [3], const int & cellid )
                {
                    if ( ( pt[0] < -790 && pt[1] < 720 ) ||
                            ( pt[0] > 1400 && pt[1] < -290 ) )
                    {
                        //pt[2] = -10; // z terena
                        pt[2] = 0.025; // mng
                        arr->SetTuple( cellid, pt );
                    }
                    else if ( ( pt[0] > 0 && pt[0] < 1400 ) && pt[1] < -153 )
                    {
                        //pt[2] = -10; // z terena
                        pt[2] = 0.025; // mng
                        arr->SetTuple( cellid, pt );
                    }
                    else if ( pt[1] < -153 )
                    {
                         pt[2] = 0.025; // mng
                        arr->SetTuple( cellid, pt );
                    }
                    else
                    {
                        //pt[2] = 30; // z terena
                        pt[2] = 0.04; // mng
                        arr->SetTuple( cellid, pt );
                    }
                },
                numberOfThreads ) );
            }

        }

        for ( int i = 0; i < numberOfThreads; ++i )
        {
            threads.push_back( boost::thread( &Interpolator::doInterpolate, &interpolators[i], i ) );
        }

        for ( int i = 0; i < numberOfThreads; ++i )
        {
            threads.at( i ).join();
        }

        end = std::chrono::system_clock::now();
        std::cout << "done interpolating values" << std::endl;
        std::chrono::duration<double> elapsed_time = end - start;
        std::cout << "cell locator duration: " <<
                  std::chrono::duration_cast<std::chrono::seconds> ( elapsed_time ).count() <<
                  " s" << std::endl;
    }
    void createStructuredGrid()
    {
        std::cout << "creating structured grid..." << std::flush;
        points = vtkSmartPointer<vtkPoints>::New();
        double dx, dy;
        dx = ( xmax - xmin ) / nx;
        dy = ( ymax - ymin ) / ny;
        std::cout << "dx = " << dx << "\t dy = " << dy << std::endl;
        double x = xmin;
        double y = ymin;

        for ( int j = 0; j <= ny; ++j )
        {
            x = xmin;

            for ( int i = 0; i <= nx; ++i )
            {
                points->InsertNextPoint( x, y, 0. );
                x += dx;
            }

            y += dy;
        }

        structuredGrid = vtkSmartPointer<vtkStructuredGrid>::New();
        structuredGrid->SetDimensions( nx + 1, ny + 1, 1 );
        structuredGrid->SetPoints( points );
        std::cout << "done" << std::endl << std::flush;
    }

private:
    vtkSmartPointer<vtkPoints> points;
    vtkSmartPointer<vtkStructuredGrid> structuredGrid;
    vtkSmartPointer<vtkPolyData> probePolyData;
    vtkSmartPointer<vtkTransformFilter> sgrid_moved;

    vtkSmartPointer<vtkTransformFilter> i_flat;
    vtkSmartPointer<vtkTransformFilter> s_flat;
    double xmin, xmax, ymin, ymax, zmin, zmax;
    const int nx, ny;
    const double tx, ty;
    const int numberOfThreads;
    const std::string meshName;
    InterpolateType itype; //! type to interpolate \see InterpolateType
};
#include <vtkSimplePointsReader.h>
#include <vtkDelaunay2D.h>
void readxyz( const std::string &full_path_name )
{
    boost::filesystem::path p( full_path_name );

    if ( !boost::filesystem::exists( p ) )
    {
        std::cout << "xyz file " << p.string() << " does not exist!" << std::endl;
        return;
    }

    ifstream fin( p.string().c_str() );
    std::cout << "Started reading " << p.string().c_str() << std::endl;

    vtkSmartPointer<vtkSimplePointsReader> reader = vtkSmartPointer<vtkSimplePointsReader>::New();
    reader->SetFileName( p.string().c_str() );
    reader->Update();

    for ( vtkIdType id = 0; id < reader->GetOutput()->GetNumberOfPoints(); ++id )
    {
        double pt[3];
        reader->GetOutput()->GetPoints()->GetPoint( id, pt );
        pt[2] = -pt[2];
        reader->GetOutput()->GetPoints()->SetPoint( id, pt );
    }

    vtkSmartPointer<vtkDelaunay2D> delaunay =
        vtkSmartPointer<vtkDelaunay2D>::New();
#if VTK_MAJOR_VERSION <= 5
    delaunay->SetInput( reader->GetOutput() );
#else
    delaunay->SetInputData( reader->GetOutput() );
#endif
    delaunay->Update();

    std::cout << reader->GetOutput()->GetNumberOfPoints() << std::endl;
    vtkSmartPointer<vtkPolyData> polydata = vtkSmartPointer<vtkPolyData>::New();
    polydata->SetPoints( reader->GetOutput()->GetPoints() );


    writeGeometry<vtkXMLPolyDataWriter> ( delaunay->GetOutput(), filePathBuilder::resultsPath() + "monai.vtp" );

    //const int grid_size = std::stoi( argv[2] ) ;
    const int grid_size = 392 ;
    double bnds[6];
    bnds[0] = 0; bnds[1] = 5.488;
    bnds[2] = 0; bnds[3] = 5.488;
    bnds[4] = -0.135; bnds[5] = 0.125;
    std::cout << "bounds: " << bnds[0] << " : " << bnds[1] << " : " << bnds[2] << " : " <<
              bnds[3] << " : " << bnds[4] << " : " << bnds[5] << std::endl;
    regularMesher rm( "monai", bnds, delaunay->GetOutput(), std::make_pair( grid_size, grid_size ),
                      std::make_pair( 0, 0 ), InterpolateType::Monai, 8 );
    rm.intepolate_mesh();
    rm.exportToXYZ( filePathBuilder::resultsPath() + "monai" + "_" +
                    std::to_string( grid_size ) + ".xyz" );


}


int main( int argc, char *argv[] )
{
    //readxyz(filePathBuilder::dataPath() + "Benchmark_2_Bathymetry.txt");
    //return 0;
    if ( argc != 3 )
    {
        std::cout << "Usage: " << argv[0]
                  << " Filename(.2dm or .vtp)  numberOfPoints(e.g. 500)" << std::endl;
        return EXIT_FAILURE;
    }

    std::string name = argv[1];
    const int grid_size = std::stoi( argv[2] ) ;

    boost::filesystem::path tmpf( name );
    boost::filesystem::path ifpath; // input filename

    vtkSmartPointer<vtkPolyData> polydata = vtkSmartPointer<vtkPolyData>::New();

    if ( boost::filesystem::exists( tmpf )  && ( tmpf.extension().string() == ".2dm" || tmpf.extension().string() == ".vtp" ) )
    {
        ifpath = tmpf;

        if ( tmpf.extension().string() == ".2dm" )
        {
            
            Reader2dm r( ifpath.stem().string(), ifpath.parent_path().string() + "/"  );
            r.createPolyData();

            if ( !r.conversionOK() )
            {
                std::cout << "conversion failed, aborting" << std::endl;
                return -1;
            }

            r.writeNodestringsToFile( filePathBuilder::resultsPath() );
            polydata->SetPoints( r.getPoints() );
            polydata->SetPolys( r.getCells() );
        }
        else // is vtp
        {
            vtkSmartPointer<vtkXMLPolyDataReader> r = vtkSmartPointer<vtkXMLPolyDataReader>::New();
            r->SetFileName(ifpath.string().c_str());
            r->Update();
            polydata->SetPoints(r->GetOutput()->GetPoints());
            polydata->SetPolys(r->GetOutput()->GetPolys());
        }
    }
    else
    {
        std::cerr << "input file doesn't exist or not 2dm or vtp file" << std::endl; return 0;
    }




    writeGeometry<vtkXMLPolyDataWriter> ( polydata, filePathBuilder::resultsPath() + ifpath.stem().string()/*name*/ + ".vtp" );
    

    double bnds[6];

    polydata->GetBounds( bnds );

    std::cout << "bounds: " << bnds[0] << " : " << bnds[1] << " : " << bnds[2] << " : " <<
              bnds[3] << " : " << bnds[4] << " : " << bnds[5] << std::endl;

//const int grid_size=8000;
    const double tx = -5456491.999777; //x translation of coordinate system
    const double ty = -5019989.003335; //y translation of coordinate system
    regularMesher rm( ifpath.stem().string()/*name*/, bnds, polydata, std::make_pair( grid_size, grid_size ),
                      std::make_pair( tx, ty ), InterpolateType::Terrain, 8 );
    rm.intepolate_mesh();
    rm.exportToXYZ( filePathBuilder::resultsPath() + ifpath.stem().string()/*name*/ + "_" +
                    std::to_string( grid_size ) + ".xyz" );

    return 0;
}
