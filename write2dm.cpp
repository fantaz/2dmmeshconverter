#include <iostream>
#include <boost/program_options.hpp>
#include <boost/filesystem.hpp>
#include <boost/filesystem/fstream.hpp>
#include <vtkXMLPolyDataReader.h>
#include <vtkSmartPointer.h>
#include <2dmWriter.h>

namespace fs = boost::filesystem;

vtkSmartPointer<vtkPolyData> readPolyData( const std::string &fname )
{
    vtkSmartPointer<vtkXMLPolyDataReader> r = vtkSmartPointer<vtkXMLPolyDataReader>::New();
    r->SetFileName( fname.c_str() );
    r->Update();
    vtkSmartPointer<vtkPolyData> polydata = vtkSmartPointer<vtkPolyData>::New();
    polydata->SetPoints( r->GetOutput()->GetPoints() );
    polydata->SetPolys( r->GetOutput()->GetPolys() );
    return polydata;
}

int main( int argc, char *argv[] )
{
    namespace po = boost::program_options;

    boost::filesystem::path ifpath; // input filename
    boost::filesystem::path ofpath; // output filename

    po::options_description desc( "Allowed options" );
    desc.add_options()
    ( "help", "produce this help message\nCreates 2dm mesh from vtp mesh." )
    ( "input", po::value<std::string>(), "full path to vtk polydata mesh" )
    ( "output", po::value<std::string>(), "full path to output 2dm mesh" )
    ;

    po::variables_map vm;
    po::store( po::parse_command_line( argc, argv, desc ), vm );

    if ( vm.count( "help" ) )
    {
        std::cout << desc << "\n";
        return 0;
    }

    if ( vm.count( "input" ) )
    {
        boost::filesystem::path tmpf( vm["input"].as<std::string>() );

        if ( boost::filesystem::exists( tmpf ) && ( tmpf.extension().string() == ".vtp" ) )
        {
            ifpath = tmpf;
        }
        else {std::cerr << "not vtp file or input file doesn't exist" << std::endl; return 0;}
    }
    else
    { std::cout << "no input file given, aborting" << std::endl; return 0; }

    if ( vm.count( "output" ) )
    {
        boost::filesystem::path tmpf( vm["output"].as<std::string>() );

        if ( tmpf.extension().string() == ".2dm" )
        {
            ofpath = tmpf;
        }
        else {std::cerr << "no 2dm file given" << std::endl; return 0;}
    }
    else
    { std::cout << "no output file given, aborting" << std::endl; return 0; }

    std::cout << " ofpath: " << ofpath.parent_path().string() << std::endl;
    std::cout << " ifpath: " << ifpath.parent_path().string() << std::endl;

    vtkSmartPointer<vtkPolyData> pdata = readPolyData( ifpath.string() );
    Writer2dm writer(ofpath.string(), pdata);
    writer.write();

    return 0;
}
