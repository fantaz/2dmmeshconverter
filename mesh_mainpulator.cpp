#include "2dmReader.h"
#include <vtkXMLPolyDataWriter.h>
#include <vtkPolyData.h>
#include <vtkTransform.h>
#include <vtkTransformPolyDataFilter.h>
#include <vtkXMLPolyDataReader.h>
#include <vtkXMLPolyDataWriter.h>
#include <vtkSimplePointsReader.h>
#include <vtkSimplePointsWriter.h>
#include <vtkDelaunay2D.h>
#include <iostream>
#include <algorithm>
#include <boost/program_options.hpp>
#include <boost/math/constants/constants.hpp>
#include <boost/filesystem.hpp>
#include <boost/thread.hpp>
#include <cmath>

#include <vtkElevationFilter.h>
template<class writerType>
void writeGeometry( vtkDataObject *geom, const std::string &fname )
{
    std::cout << "writing " << fname << "....";
    writerType *writer =  writerType::New();
    writer->SetFileName( fname.c_str() );
#if VTK_MAJOR_VERSION <= 5
    writer->SetInput( geom );
#else
    writer->SetInputData( geom );
#endif
    writer->SetDataModeToBinary();
    //writer->SetDataModeToAscii();
    writer->Write();
    writer->Delete();
    std::cout << "done." << std::endl;
}

vtkSmartPointer<vtkPolyData> readPolyData( const std::string &fname )
{
    vtkSmartPointer<vtkXMLPolyDataReader> r = vtkSmartPointer<vtkXMLPolyDataReader>::New();
    r->SetFileName( fname.c_str() );
    r->Update();
    vtkSmartPointer<vtkPolyData> polydata = vtkSmartPointer<vtkPolyData>::New();
    polydata->SetPoints( r->GetOutput()->GetPoints() );
    polydata->SetPolys( r->GetOutput()->GetPolys() );
    return polydata;
}

int main( int argc, char *argv[] )
{
    namespace po = boost::program_options;

    boost::filesystem::path ifpath; // input filename
    boost::filesystem::path ofpath; // output filename

    po::options_description desc( "Allowed options" );
    desc.add_options()
    ( "help", "produce this help message\n Creates STL on interpolated mesh." )
    ( "input", po::value<std::string>(), "full path to input vtk mesh" )
    ( "output", po::value<std::string>(), "full path to vtk mesh output" )
    ;
    po::variables_map vm;
    po::store( po::parse_command_line( argc, argv, desc ), vm );

    if ( vm.count( "help" ) )
    {
        cout << desc << "\n";
        return 0;
    }

    if ( vm.count( "input" ) )
    {
        boost::filesystem::path tmpf( vm["input"].as<std::string>() );

        if ( boost::filesystem::exists( tmpf ) &&
                ( tmpf.extension().string() == ".xyz"  || tmpf.extension().string() == ".2dm" ||
                  tmpf.extension().string() == ".vtp" ) )
        {
            ifpath = tmpf;
        }
        else {std::cerr << "not vtk file or input file doesn't exist" << std::endl; return 0;}
    }
    else
    { std::cout << "no input file given, aborting" << std::endl; return 0; }

    if ( vm.count( "output" ) )
    {
        boost::filesystem::path tmpf( vm["output"].as<std::string>() );

        if ( tmpf.extension().string() == ".vtp"  || tmpf.extension().string() == ".xyz" )
        {
            ofpath = tmpf;
        }
        else {std::cerr << "not vtk or xyz file or file doesn't exist" << std::endl; return 0;}
    }
    else
    { std::cout << "no output file given, aborting" << std::endl; return 0; }



    std::cout << " ofpath: " << ofpath.parent_path().string() << std::endl;
    std::cout << " ifpath: " << ifpath.parent_path().string() << std::endl;
    std::cout << " input stem: " << ifpath.stem().string() << std::endl;
    std::cout << " out stem: " << ofpath.stem().string() << std::endl;

    const double tx = -5456491.999777; //x translation of coordinate system
    const double ty = -5019989.003335; //y translation of coordinate system

    if ( ifpath.extension().string() == ".vtp" )
    {
        vtkSmartPointer<vtkPolyData> indata = readPolyData( ifpath.string() );

        vtkSmartPointer<vtkTransform> translator = vtkSmartPointer<vtkTransform>::New();
        translator->Translate( tx, ty, 0. );
        translator->Update();

        vtkSmartPointer<vtkTransformPolyDataFilter> transformFilter =
            vtkSmartPointer<vtkTransformPolyDataFilter>::New();

        transformFilter->SetTransform( translator );
        transformFilter->SetInputData( indata );
        transformFilter->Update();

        if ( ofpath.extension().string() == ".vtp" )
        {
            writeGeometry<vtkXMLPolyDataWriter> ( transformFilter->GetOutput(),
                                                  ofpath.string() );
        }
        else if ( ofpath.extension().string() == ".xyz" )
        {
            vtkSmartPointer<vtkSimplePointsWriter> writer = vtkSmartPointer<vtkSimplePointsWriter>::New();
            writer->SetFileName( ofpath.c_str() );
            writer->SetInputConnection( transformFilter->GetOutputPort() );
            writer->Write();
        }

    }
    else if ( ifpath.extension().string() == ".xyz" )
    {
        vtkSmartPointer<vtkSimplePointsReader> reader = vtkSmartPointer<vtkSimplePointsReader>::New();
        reader->SetFileName( ifpath.string().c_str() );
        reader->Update();
        double bounds[6];
        reader->GetOutput()->GetBounds( bounds );
        vtkSmartPointer<vtkDelaunay2D> del = vtkSmartPointer<vtkDelaunay2D>::New();
        del->SetInputConnection( reader->GetOutputPort() );
        del->Update();


        vtkSmartPointer<vtkElevationFilter> elevationFilter = vtkSmartPointer<vtkElevationFilter>::New();
        elevationFilter->SetInputConnection( del->GetOutputPort() );
        elevationFilter->SetLowPoint( 0.0, 0.0, bounds[4] );
        elevationFilter->SetHighPoint( 0.0, 0.0, bounds[5] );
        elevationFilter->Update();

        vtkSmartPointer<vtkTransform> transform = vtkSmartPointer<vtkTransform>::New();
        //transform->RotateWXYZ(double angle, double x, double y, double z);
        //transform->Translate(-9.4,-1,0);
        const double _pi = boost::math::constants::pi<double>();
        double deg = std::atan( 0.05 / 0.115 ) * 180. / _pi;
        std::cout << deg << std::endl;
        transform->RotateWXYZ( deg, 0, 0, 1 );
        //transform->Translate(9.4,1,0);
        double d[3];
        transform->GetPosition( d );
        std::cout << d[0] << " : " << d[1] << " : " << d[2] << "\n";
        vtkSmartPointer<vtkTransformPolyDataFilter> transformFilter =
            vtkSmartPointer<vtkTransformPolyDataFilter>::New();

        transformFilter->SetTransform( transform );
        //transformFilter->SetInputData( indata );
        transformFilter->SetInputConnection( elevationFilter->GetOutputPort() );
        transformFilter->Update();

        writeGeometry<vtkXMLPolyDataWriter> ( transformFilter->GetOutput(),
                                              ofpath.string() );
    }

}
