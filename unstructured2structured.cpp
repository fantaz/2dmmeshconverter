
#include <iostream>
#include "pathconfig.h"
#include "vtkUnstructuredGridReader.h"
#include "vtkUnstructuredGrid.h"
#include "vtkSmartPointer.h"
#include "vtkXMLStructuredGridWriter.h"
#include "vtkStructuredGrid.h"
#include "vtkXMLUnstructuredGridWriter.h"
#include "vtkXMLPolyDataWriter.h"
#include <vtkPolyData.h>
#include "vtkPointData.h"
#include <vtkDataSetSurfaceFilter.h>
#include <vtkCellLocator.h>
#include <vtkIdTypeArray.h>
#include <vtkSelectionNode.h>
#include <vtkInformation.h>
#include <vtkExtractSelection.h>
#include <vtkSelection.h>
#include <vtkTriangleFilter.h>
#include <vtkSTLWriter.h>
#include <vtkDecimatePro.h>
#include <sstream>
#include <cctype>
#include <chrono>
#include <boost/thread.hpp>
#include "interpolator.h"

template<class writerType>
void writeGeometry ( vtkDataObject* geom, const std::string fname )
{
    std::cout<<"writing "<<fname<<"....";
    writerType* writer =  writerType::New();
    writer->SetFileName ( fname.c_str() );
#if VTK_MAJOR_VERSION <= 5
    writer->SetInput ( geom );
#else
    writer->SetInputData ( geom );
#endif
    writer->SetDataModeToBinary();
    writer->Write();
    writer->Delete();
    std::cout<<"done."<<std::endl;
}
/*
class Interpolator
{
private:
    vtkDataArray* arrcoords;
    const int numberOfThreads;
    vtkSmartPointer<vtkCellLocator> cellLocator;
    vtkPolyData* source;
public:
    Interpolator(vtkDataArray* _arrcoords, vtkPolyData *_sf, const int nthreads) :
        arrcoords(_arrcoords),
        numberOfThreads(nthreads),
        source(_sf)
    {
        cellLocator = vtkSmartPointer<vtkCellLocator>::New();
        cellLocator->SetDataSet ( source );
        cellLocator->BuildLocator();
        cellLocator->Update();
    }
    void doInterpolate ( const int &threadId )
    {
        int elements = arrcoords->GetNumberOfTuples();
        int sz = elements/numberOfThreads;
        int b = sz*threadId;
        int e = b+sz-1;

        if ( threadId == numberOfThreads-1 ) {
            e = elements-1;
        }
        //std::cout<<"elements = "<<elements<<std::endl;
        //std::cout<<"thread = "<<threadId<<"\t"<<b<<":"<<e<<"\t sz= "<<e-b+1<<std::endl;
        //std::cout<<"\tstarting thread id "<<threadId<<"with point range "<<b<<":"<<e<<std::endl<<std::flush;
        {   std::stringstream stream;
            stream <<"\tstarting thread id "<<threadId<<" with point range: "<<b<<":"<<e<<std::endl;
            std::cout << stream.str()<<std::flush;
        }
        int subId;
        double t, xyz[3], pcoords[3];
        double rayStart[3], rayEnd[3];

        for ( int i=b; i<=e; ++i )
        {
            arrcoords->GetTuple(i,rayStart);
            rayStart[2] += 1000.0;
            arrcoords->GetTuple(i,rayEnd);
            rayEnd[2] -= 1000.0;

            if ( cellLocator->IntersectWithLine (
                        rayStart,
                        rayEnd,
                        0.0001,
                        t,
                        xyz,
                        pcoords,
                        subId ) )
            {
                rayStart[2] = xyz[2];
                arrcoords->SetTuple ( i,rayStart );
            }
            else
            {
                //Neumrezeni rubovi dobiju posebnu visinu
                {
                    rayStart[2] = 80;
                    arrcoords->SetTuple ( i,rayStart );
                }
            }
        }
        {   std::stringstream stream;
            //std::cout<<"\tdone with thread id: "<<threadId<<std::endl<<std::flush;
            stream<<"\tdone with thread id: "<<threadId<<std::endl;
            std::cout<<stream.str()<<std::flush;
        }
    }

};
*/

//! \brief stavlja vrijednost atributa iz pd u z koordinatu u arrcoords
void attributeToZ(vtkPointData* pd, vtkDataArray* arrcoords)
{
    std::cout << " contains point data with "
              << pd->GetNumberOfArrays()
              << " arrays." << std::endl;

    for ( int i = 0; i < pd->GetNumberOfArrays(); i++ )
    {
        std::cout << "\tArray " << i
                  << " is named "
                  << ( pd->GetArrayName ( i ) ? pd->GetArrayName ( i ) : "NULL" )
                  << std::endl;
    }
    int br = pd->SetActiveAttribute ( "H",vtkDataSetAttributes::SCALARS );
    vtkDataArray* Parr = pd->GetArray ( br );

    double pcoords[3];

    for ( int i=0; i< arrcoords->GetNumberOfTuples(); ++i )
    {
        arrcoords->GetTuple ( i,pcoords );
        pcoords[2] = Parr->GetTuple1 ( i );
        arrcoords->SetTuple ( i,pcoords );
    }

}

//! \brief convert mesh to polydata
vtkSmartPointer<vtkDataSetSurfaceFilter> convertToPolyData(vtkDataObject* obj)
{
    vtkSmartPointer<vtkDataSetSurfaceFilter> surfaceFilter =  vtkSmartPointer<vtkDataSetSurfaceFilter>::New();
    surfaceFilter->SetInputData ( obj );
    surfaceFilter->Update();
    return surfaceFilter;
}

//! \brief creates structuredGrid based on bounds and number of points
vtkSmartPointer<vtkStructuredGrid> createStructuredGrid(double bounds[6], int nx,int ny)
{
    std::cout<<"creating structured grid..."<<std::flush;
    std::cout<<"bounds: "<< bounds[0]<<" : "<<bounds[1]<<" : "<<bounds[2]<<" : "<<
             bounds[3]<<" : "<<bounds[4]<<" : "<<bounds[5]<<std::endl;
    double xmax,ymax,xmin,ymin;
    xmin=bounds[0];
    xmax=bounds[1];
    ymin=bounds[2];
    ymax=bounds[3];
    vtkSmartPointer<vtkPoints> points = vtkSmartPointer<vtkPoints>::New();
    double dx, dy;
    dx = ( xmax-xmin ) /nx;
    dy = ( ymax-ymin ) /ny;
    std::cout<<"dx = "<<dx<<"\t dy = "<<dy<<std::endl;
    double x = xmin;
    double y = ymin;

    for ( int j=0; j<=ny; ++j )
    {
        x=xmin;

        for ( int i=0; i<=nx; ++i )
        {
            points->InsertNextPoint ( x, y, 0. );
            x+=dx;
        }
        y+=dy;
    }
    vtkSmartPointer<vtkStructuredGrid> structuredGrid = vtkSmartPointer<vtkStructuredGrid>::New();
    structuredGrid->SetDimensions ( nx+1,ny+1,1 );
    structuredGrid->SetPoints ( points );
    std::cout<<"done"<<std::endl<<std::flush;
    return structuredGrid;
}

void interpolatePolyDataToStructuredGrid(vtkPolyData* pdata, vtkDataArray* target, int numberOfThreads)
{
    std::cout<<"interpolating values ..."<<std::endl;
    std::chrono::time_point<std::chrono::system_clock> start, end;
    start = std::chrono::system_clock::now();
    std::vector<Interpolator> interpolators;
    std::vector<boost::thread> threads;

    for(int i=0; i<numberOfThreads; ++i)
    {
        interpolators.push_back(Interpolator(target,pdata,
            [](vtkDataArray* arr, double (& pt) [3], const int& cellid){
                pt[2] = 80;
                arr->SetTuple ( cellid,pt );
            },numberOfThreads));    
    }

    for(int i=0; i<numberOfThreads; ++i)
    {
        threads.push_back(boost::thread (&Interpolator::doInterpolate, &interpolators[i], i));
    }

    for(int i=0; i<numberOfThreads; ++i)
    {
        threads.at(i).join();
    }

    end = std::chrono::system_clock::now();
    std::cout<<"done interpolating values"<<std::endl;
    std::chrono::duration<double> elapsed_time = end-start;
    std::cout<<"cell locator duration: "<<
             std::chrono::duration_cast<std::chrono::seconds> ( elapsed_time ).count() <<
             " s"<<std::endl;
}

//! \brief get points (and cells) that ARE in original mesh (buildings, etc.)
//! Points that have Z > Threshold( i.e. 50) ARE NOT in original mesh,
//! and therefore are SELECTED
std::tuple<vtkSmartPointer<vtkExtractSelection>,vtkSmartPointer<vtkUnstructuredGrid>>
        extractMesh(vtkPolyData* mesh)
{
    vtkSmartPointer<vtkIdTypeArray> ids = vtkSmartPointer<vtkIdTypeArray>::New();
    ids->SetNumberOfComponents(1);
    vtkDataArray* polycoords = mesh->GetPoints()->GetData();
    double ptcoords[3];
    for ( vtkIdType i=0; i< polycoords->GetNumberOfTuples(); ++i )
    {
        polycoords->GetTuple ( i,ptcoords );
        if (ptcoords[2]> 29.9)
        {
            ids->InsertNextValue(i);
        }
    }

    vtkSmartPointer<vtkSelectionNode> selectionNode =
        vtkSmartPointer<vtkSelectionNode>::New();
    selectionNode->SetFieldType(vtkSelectionNode::POINT);
    selectionNode->SetContentType(vtkSelectionNode::INDICES);
    selectionNode->SetSelectionList(ids);
    selectionNode->GetProperties()->Set(vtkSelectionNode::CONTAINING_CELLS(), 1);

    vtkSmartPointer<vtkSelection> selection =
        vtkSmartPointer<vtkSelection>::New();
    selection->AddNode(selectionNode);

    vtkSmartPointer<vtkExtractSelection> extractSelection =
        vtkSmartPointer<vtkExtractSelection>::New();

    extractSelection->SetInputData(0, mesh);
    extractSelection->SetInputData(1, selection);
    extractSelection->Update();

    // Get points that are NOT in the selection
    selectionNode->GetProperties()->Set(vtkSelectionNode::INVERSE(), 1); //invert the selection
    extractSelection->Update();

    vtkSmartPointer<vtkUnstructuredGrid> notSelected =
        vtkSmartPointer<vtkUnstructuredGrid>::New();
    notSelected->ShallowCopy(extractSelection->GetOutput());

    std::cout << "There are " << notSelected->GetNumberOfPoints()
    << " points NOT in the selection." << std::endl;
    std::cout << "There are " << notSelected->GetNumberOfCells()
    << " cells NOT in the selection." << std::endl;
    return std::make_tuple(extractSelection,notSelected);
}

void exportToSTL(vtkSmartPointer<vtkUnstructuredGrid> grid, const std::string& stlout)
{
    vtkSmartPointer<vtkDataSetSurfaceFilter> surfaceFilter = convertToPolyData(grid);
    vtkSmartPointer<vtkTriangleFilter> tri = vtkSmartPointer<vtkTriangleFilter>::New();
    tri->SetInputConnection(surfaceFilter->GetOutputPort());
    tri->Update();
    
    vtkSmartPointer<vtkDecimatePro> deci = vtkSmartPointer<vtkDecimatePro>::New();
    deci->SetInputConnection(tri->GetOutputPort());
    deci->PreserveTopologyOn();
    deci->Update();
    
    vtkSmartPointer<vtkSTLWriter> exporter= vtkSmartPointer<vtkSTLWriter>::New();
    exporter->SetInputConnection(tri->GetOutputPort());
    exporter->SetFileName ( stlout.c_str() );
    exporter->Update();
    exporter->Write();
}
int main( )
{
    std::string fname = filePathBuilder::dataPath() + "simres-2240.00.vtk";
    //std::string fname = filePathBuilder::dataPath() + "nesto2.vtk";
    vtkSmartPointer<vtkUnstructuredGridReader> reader = vtkSmartPointer<vtkUnstructuredGridReader>::New();
    reader->SetFileName ( fname.c_str() );
    reader->Update();


    attributeToZ(reader->GetOutput()->GetPointData(),reader->GetOutput()->GetPoints()->GetData());

//     writeGeometry<vtkXMLUnstructuredGridWriter> ( reader->GetOutput(),
//                                                   filePathBuilder::dataPath() + "P.vtu" );


    vtkSmartPointer<vtkDataSetSurfaceFilter> surfaceFilter = convertToPolyData(reader->GetOutput());
    vtkPolyData* polydata = surfaceFilter->GetOutput();
//     writeGeometry<vtkXMLPolyDataWriter> ( polydata,
//                                           filePathBuilder::dataPath() + "P.vtp" );

    double bnds[6];
    polydata->GetBounds ( bnds );
    int numx {2000};
    int numy {2000};

    vtkSmartPointer<vtkStructuredGrid> structuredGrid = createStructuredGrid(bnds,numx,numy);

    int threadNum = 8;
    interpolatePolyDataToStructuredGrid(polydata,structuredGrid->GetPoints()->GetData(),threadNum);

    //writeGeometry<vtkXMLStructuredGridWriter> ( structuredGrid, filePathBuilder::dataPath() + "P.vts" );

    vtkSmartPointer<vtkDataSetSurfaceFilter> interp_surface = convertToPolyData(structuredGrid);
    vtkPolyData* interp_polydata = interp_surface->GetOutput();

   //writeGeometry<vtkXMLPolyDataWriter> ( interp_polydata, filePathBuilder::dataPath() + "P_interp.vtp" );

    vtkSmartPointer<vtkExtractSelection> extractSelection;
    vtkSmartPointer<vtkUnstructuredGrid> notSelected;
    std::tie(extractSelection,notSelected) = extractMesh(interp_polydata);
//     writeGeometry<vtkXMLUnstructuredGridWriter> ( notSelected, filePathBuilder::dataPath() +
//             "Zb_not_Sel.vtu" );
    exportToSTL(notSelected,filePathBuilder::dataPath() + "sim_H_2240_novi.stl");
    return 0;
}
