#ifndef _charter_h 
#define _charter_h
#include <vtkVersion.h>
#include <vtkSmartPointer.h>

#include <vtkChartXY.h>
#include <vtkContextScene.h>
#include <vtkContextView.h>
#include <vtkFloatArray.h>
#include <vtkPlotPoints.h>
#include <vtkRenderWindow.h>
#include <vtkRenderWindowInteractor.h>
#include <vtkRenderer.h>
#include <vtkTable.h>
#include <vtkColor.h>
class Charter
{
public:
    Charter()
    {
        // Set up a 2D scene, add an XY chart to it
        view = vtkSmartPointer<vtkContextView>::New();
        view->GetRenderer()->SetBackground ( 1.0, 1.0, 1.0 );
        view->GetRenderWindow()->SetSize ( 400, 300 );

        chart = vtkSmartPointer<vtkChartXY>::New();
        view->GetScene()->AddItem ( chart );
        chart->SetShowLegend ( true );
    }
    void addplot ( const std::vector<double> &x, const std::vector<double> &y, std::string name,const double& line_width = 1.,const vtkColor4ub& /*c*/=vtkColor4ub(0,0,0,255))
    {
        vtkSmartPointer<vtkFloatArray> arrX = vtkSmartPointer<vtkFloatArray>::New();
        arrX->SetName ( "X Axis" );
        vtkSmartPointer<vtkFloatArray> arrY = vtkSmartPointer<vtkFloatArray>::New();
        arrY->SetName ( name.c_str() );
        vtkSmartPointer<vtkTable> table = vtkSmartPointer<vtkTable>::New();
        for ( const auto& v : x ) { arrX->InsertNextValue(v); }
        for ( const auto& v : y ) { arrY->InsertNextValue(v); }
        std::cout<<x.size()<<std::endl;
        table->AddColumn ( arrX );
        table->AddColumn ( arrY );
        
        vtkPlot *points = chart->AddPlot ( vtkChart::LINE );
#if VTK_MAJOR_VERSION <= 5
        points->SetInput ( table, 0, 1 );
#else
        points->SetInputData ( table, 0, 1 );
#endif
        //points->SetColor ( 0, 0, 0, 255 );
        //points->SetColor(c.GetRed(),c.GetGreen(),c.GetBlue(),c.GetAlpha());
        points->SetWidth ( line_width );
        vtkPlotPoints::SafeDownCast ( points )->SetMarkerStyle ( vtkPlotPoints::CROSS );
    }
    void draw()
    {
        view->GetRenderWindow()->SetMultiSamples ( 0 );
        view->GetInteractor()->Initialize();
        view->GetInteractor()->Start();

    }
private:
    vtkSmartPointer<vtkContextView> view;
    vtkSmartPointer<vtkChartXY> chart;
};
#endif //_charter_h

