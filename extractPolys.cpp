#include <boost/program_options.hpp>
#include <boost/filesystem.hpp>
#include "2dmReader.h"

int main( int ac, char *av[] )
{
    
    namespace po = boost::program_options;
    namespace fs  = boost::filesystem;
    
    boost::filesystem::path ifpath; // input filename
    int polysNumber{0};
    po::options_description desc( "Options" );
    desc.add_options()
    ( "help,h", "Usage: extractPolys --input " )
    ( "input,i", po::value<std::string>(), "Input file" )
    ( "number,n", po::value<int>(&polysNumber), "Number of polys to extract" )
    ;
    
    po::variables_map vm;
    po::store( po::parse_command_line( ac, av, desc ), vm );
    
    if ( vm.count( "help" ) )
    {
        cout << desc << "\n";
        return 0;
    }

    if ( vm.count( "input" ) )
    {
        boost::filesystem::path tmpf( vm["input"].as<std::string>() );

        if ( boost::filesystem::exists( tmpf ) && tmpf.extension().string() == ".2dm" )
        {
            ifpath = tmpf;
        }
        else {std::cerr << "not 2dm file or input file doesn't exist" << std::endl; return 0;}
    }
    else
    {
        std::cout << "no input file given, aborting" << std::endl;
        return 0;
    }
    
    //Reader2dm reader( ifpath.stem().string(), ifpath.parent_path().string() + "/" );
    
    
    return 0;
}

