#include <vtkVersion.h>
#include "2dmReader.h"
#include <vtkPolyData.h>
#include <vtkPointData.h>

#include <vtkProbeFilter.h>
#include <vtkTransform.h>
#include <vtkTransformFilter.h>
#include <vtkElevationFilter.h>
#include <vtkWarpScalar.h>

#include <vtkXMLPolyDataWriter.h>

#include <vtkStructuredGrid.h>
#include <vtkXMLStructuredGridWriter.h>
#include <vtkXMLStructuredGridReader.h>
#include <vtkSimplePointsWriter.h>
#include <vtkDecimatePro.h>
#include <vtkDistancePolyDataFilter.h>


#include "pathconfig.h"
#include <fstream>
#include <iostream>
#include <sstream>
#include <cctype>
using std::ifstream;
#define VTK_CREATE(type, name) \
    vtkSmartPointer<type> name = vtkSmartPointer<type>::New();
template<class writerType>
void writeGeometry ( vtkDataObject* geom, const std::string fname )
{
    std::cout<<"writing "<<fname<<"....";
    writerType* writer =  writerType::New();
    writer->SetFileName ( fname.c_str() );
#if VTK_MAJOR_VERSION <= 5
    writer->SetInput ( geom );
#else
    writer->SetInputData ( geom );
#endif
    writer->SetDataModeToBinary();
    writer->Write();
    writer->Delete();
    std::cout<<"done."<<std::endl;
}

void decimate_n_compute_dist(vtkSmartPointer<vtkPolyData> poly, const std::string& n,
                             const int& reduction)
{
    
    VTK_CREATE ( vtkDecimatePro,deci );
    deci->SetInputData ( poly );
    deci->SetTargetReduction ( reduction/100.00 ); 
    deci->SplittingOff();
    deci->Update();
    writeGeometry<vtkXMLPolyDataWriter> ( deci->GetOutput(),filePathBuilder::resultsPath() +
    n +"_deci_" + std::to_string(reduction)+".vtp" );
    
    VTK_CREATE(vtkDistancePolyDataFilter,distanceFilter);
    distanceFilter->SetInputData( 0, poly );
    distanceFilter->SetInputConnection( 1, deci->GetOutputPort() );
    distanceFilter->Update();
    writeGeometry<vtkXMLPolyDataWriter> ( distanceFilter->GetOutput(),filePathBuilder::resultsPath() +
    n +"_dist_" + std::to_string(reduction)+".vtp" );
}

int main()
{
    std::string name = "MrtviKanal_37_refinezid_03_3";
    Reader2dm r ( filePathBuilder::dataPath() +name +".2dm" );

    // Add the grid points to a polydata object
    VTK_CREATE ( vtkPolyData,polydata );
    polydata->SetPoints ( r.getPoints() );
    polydata->SetPolys ( r.getCells() );

    writeGeometry<vtkXMLPolyDataWriter> ( polydata,filePathBuilder::resultsPath() +name +".vtp" );
    //decimate_n_compute_dist(polydata, name, 50);
    decimate_n_compute_dist(polydata, name, 75);
    
}
