#ifndef _2dmReader_h
#define _2dmReader_h
#include <string>
#include <set>
#include <map>
#include <vtkSmartPointer.h>
#include <vtkPoints.h>
#include <vtkCellArray.h>
#include <vtkPolyLine.h>
#include <vtkPolygon.h>
#include <vtkPolyData.h>
#include <sstream>
#include <fstream>
#include <limits>
#include<cmath>
#include <boost/concept_check.hpp>
#include "pathconfig.h"
#include <boost/filesystem.hpp>

class node
{
public:
    node() : x ( 0 ), y ( 0 ), z ( 0 ) {}
    node ( double _x, double _y, double _z ) : x ( _x ), y ( _y ), z ( _z ) {}
    node ( const node& other ) : x ( other.x ), y ( other.y ), z ( other.z ) {}
    node& operator = ( const node& other ) {x = other.x; y = other.y; z = other.z; return *this;}
    double x,y,z;
};

class cell
{
public:
    cell() {}
    //std::string type;

};
class tricell : public cell
{
public:
    tricell ( int _id1, int _id2, int _id3 ) : id1 ( _id1 ), id2 ( _id2 ), id3 ( _id3 ) {};
    int id1, id2, id3;
};
class quadcell : public cell
{
public:
    quadcell ( int _id1, int _id2, int _id3, int _id4 ) :
        id1 ( _id1 ), id2 ( _id2 ), id3 ( _id3 ), id4 ( _id4 ) {};
    int id1, id2, id3, id4;
};

class Reader2dm
{
public:
    Reader2dm ( const std::string &fname, const std::string &pth=filePathBuilder::dataPath(), const std::string _ext="2dm" ) :
        converted2PolyData ( false ), path ( pth ), file_name_no_ext ( fname ), ext ( _ext )
    {    }
    void createPolyData()
    {
        points = vtkSmartPointer<vtkPoints>::New();

        if ( !doRead2dm() )
        {converted2PolyData = false; return;}

        polys = vtkSmartPointer<vtkCellArray>::New();

        for ( auto& item : trimap )
        {

            polys->InsertNextCell ( 3 );
            polys->InsertCellPoint ( node2ptmap[item.second.id1] );
            polys->InsertCellPoint ( node2ptmap[item.second.id2] );
            polys->InsertCellPoint ( node2ptmap[item.second.id3] );
        }

        for ( auto& item : quadmap )
        {
            polys->InsertNextCell ( 4 );
            polys->InsertCellPoint ( node2ptmap[item.second.id1] );
            polys->InsertCellPoint ( node2ptmap[item.second.id2] );
            polys->InsertCellPoint ( node2ptmap[item.second.id3] );
            polys->InsertCellPoint ( node2ptmap[item.second.id4] );
        }

        if ( !nodestrings.empty() )
        {
            nodestringPolylines = vtkSmartPointer<vtkCellArray>::New();
            for ( const auto& ns : nodestrings )
            {
                vtkSmartPointer<vtkPolyLine> polyLine = vtkSmartPointer<vtkPolyLine>::New();
                //vtkSmartPointer<vtkPolygon> polygon = vtkSmartPointer<vtkPolygon>::New();
                polyLine->GetPointIds()->SetNumberOfIds ( ns.size() );
                //polygon->GetPointIds()->SetNumberOfIds ( ns.size() );

                for ( int i=0; i< ( int ) ns.size(); ++i )
                {
                    polyLine->GetPointIds()->SetId ( i,node2ptmap[ns[i]] );
                    //polygon->GetPointIds()->SetId ( i,node2ptmap[ns[i]] );
                }
                nodestringPolylines->InsertNextCell(polyLine);
            }
        }

        converted2PolyData=true;
    }
    
    void writeNodestringsToFile ( const std::string &dir_name = filePathBuilder::dataPath() )
    {
        int prec = std::numeric_limits<double>::digits10+2; // generally 17
        int exponent_digits = std::log10 ( std::numeric_limits<double>::max_exponent10 ) +1; // generally 3
        int exponent_sign   = 1; // 1.e-123
        int exponent_symbol = 1; // 'e' 'E'
        int digits_sign = 1;
        int digits_dot = 1; // 1.2

        int division_extra_space = 1;
        int width = prec + exponent_digits + digits_sign + exponent_sign + digits_dot + exponent_symbol + division_extra_space;

        if ( !nodestrings.empty() )
        {
            int nsId = 0;
            std::cout<<"writing "<<nodestrings.size() <<" nodestrings to files"<<std::endl;

            for ( const auto& ns : nodestrings )
            {
                std::string fname = dir_name + file_name_no_ext +"_"+std::to_string ( nsId ) + ".txt";
                std::cout<<fname<<std::endl;
                node pt;
                ofstream fout ( fname );

                for ( const auto& n : ns )
                {
                    pt = ptmap[n];
                    fout<<std::setprecision ( prec ) << std::setw ( width ) << pt.x<<"\t"<<
                        std::setprecision ( prec ) << std::setw ( width ) <<pt.y<<"\t"<<
                        std::setprecision ( prec ) << std::setw ( width ) <<pt.z<<std::endl;
                }

                fout.close();
                nsId++;
            }

        }
    }
    vtkSmartPointer<vtkPoints> getPoints() const { return points; }
    vtkSmartPointer<vtkCellArray> getCells() const { return polys; }
    vtkSmartPointer<vtkCellArray> getNodestringPolylines() const { return nodestringPolylines; }
    vtkSmartPointer<vtkCellArray> getLast_N_NodestringPolygons( const int& poly_num)
    {
        vtkSmartPointer<vtkCellArray> extractPolygons = vtkSmartPointer<vtkCellArray>::New();
        assert(nodestrings.size >= poly_num);
        std::vector<std::vector<int>> nstrs(nodestrings.end()-poly_num, nodestrings.end());
        std::cout<<"n nodestrings: "<<nstrs.size()<<std::endl;
        for ( const auto& ns : nstrs )
            {
                vtkSmartPointer<vtkPolygon> polygon = vtkSmartPointer<vtkPolygon>::New();
                polygon->GetPointIds()->SetNumberOfIds ( ns.size() );

                for ( int i=0; i< ( int ) ns.size(); ++i )
                {
                    polygon->GetPointIds()->SetId ( i,node2ptmap[ns[i]] );
                }
                extractPolygons->InsertNextCell(polygon);
            }
            return extractPolygons;
    }
    
    std::vector<vtkSmartPointer<vtkPolyData>> getLastNPolygons(const int& poly_num)
    {
        std::vector<vtkSmartPointer<vtkPolyData>> poly_vec;
        std::vector<std::vector<int>> nstrs(nodestrings.end()-poly_num, nodestrings.end());
        
        for ( const auto& ns : nstrs )
        {
            vtkSmartPointer<vtkPolyData> polydata = vtkSmartPointer<vtkPolyData>::New();
            vtkSmartPointer<vtkCellArray> cells = vtkSmartPointer<vtkCellArray>::New();
            vtkSmartPointer<vtkPoints> pts = vtkSmartPointer<vtkPoints>::New();
            vtkSmartPointer<vtkPolygon> polygon = vtkSmartPointer<vtkPolygon>::New();
            polygon->GetPointIds()->SetNumberOfIds ( ns.size() );
            pts->SetNumberOfPoints(ns.size());
            double coo[3];
            for ( int i=0; i< ( int ) ns.size(); ++i )
            {
                points->GetPoint(node2ptmap[ns[i]],coo);
                pts->SetPoint(i,coo);
                polygon->GetPointIds()->SetId ( i,i );
            }
            cells->InsertNextCell(polygon);
            polydata->SetPoints(pts);
            polydata->SetPolys(cells);
            poly_vec.push_back(polydata);
        }
        
        return poly_vec;
    }
    bool conversionOK() const {return converted2PolyData;}
    bool hasNodestrings() const {return !nodestrings.empty();}
    int  getNumberOfNodestrings() const {return nodestrings.size();}
private:
    bool doRead2dm()
    {
        std::string fullpath = path+file_name_no_ext+"." + ext;
        boost::filesystem::path p ( fullpath );

        if ( !boost::filesystem::exists ( p ) )
        {
            std::cout<<"2dm file " << p.string() <<" does not exist!"<<std::endl;
            return false;
        }

        ifstream fin ( p.string().c_str() );
        std::string str;
        int celids =0;
        int ptid = 0;
        std::vector<int> ns; //! \brief ONE nodestring
        std::cout<<"Started reading "<<p.string().c_str() <<std::endl;

        while ( fin>>str )
        {
            if ( str== "E3T" )
            {
                ++celids;
                vtkIdType id, a, b, c, one;
                fin >> id >> a >> b >> c >> one;
                trimap.insert ( std::make_pair ( id,tricell ( a,b,c ) ) );
            }
            else if ( str== "E4Q" )
            {
                vtkIdType id, a, b, c, d, one;
                fin >> id >> a >> b >> c >>d >> one;
                quadmap.insert ( std::make_pair ( id,quadcell ( a,b,c,d ) ) );
                ++celids;
            }

            if ( str=="ND" )
            {
                int id;
                double x,y,z;
                fin>>id;
                fin>>x;
                fin>>y;
                fin>>z;
                points->InsertNextPoint ( x, y, z );
                ptmap.insert ( std::make_pair ( id,node ( x,y,z ) ) );
                node2ptmap.insert ( std::make_pair ( id,ptid ) );
                ++ptid;
            }

            if ( str == "NS" )
            {
                std::string ids_as_string;
                std::getline ( fin, ids_as_string ); // read the rest of the line
                std::istringstream input ( ids_as_string );
                int id;

                while ( input >> id )
                {
                    ns.push_back ( id ); // convert strings (ids) to int
                }

                if ( ns.back() < 0 ) // nodestring is now read, put it in vector of nodestrings
                {
                    ns.back() =ns.back() * ( -1 );
                    nodestrings.push_back ( ns );
                    ns.clear();
                }
            }
        }

        std::cout<<"there are "<<celids<<" cells"<<std::endl;
        std::cout<<"there are "<<points->GetNumberOfPoints()<<" points"<<std::endl;
        return true;
    }
private:
    bool converted2PolyData;
    vtkSmartPointer<vtkPoints> points;
    vtkSmartPointer<vtkCellArray> polys;
    
    vtkSmartPointer<vtkCellArray> nodestringPolylines;

    const std::string path;
    const std::string file_name_no_ext;
    const std::string ext;

    std::map<int, node> ptmap;
    std::map<int, int> node2ptmap; // 1. je ptmap key, 2. je redni broj
    std::map<int, quadcell> quadmap;
    std::map<int, tricell> trimap;
    std::vector<std::vector<int>> nodestrings; // list of nodestrings
};

#endif //_2dmReader_h
