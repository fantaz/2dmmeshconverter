#include <iostream>
#include <cassert>
#include <vtkUnstructuredGridReader.h>
#include <vtkXMLUnstructuredGridWriter.h>
#include <vtkXMLUnstructuredGridReader.h>
#include <vtkXMLPolyDataWriter.h>
#include <vtkXMLPolyDataReader.h>
#include <vtkUnstructuredGrid.h>
#include <vtkSmartPointer.h>

#include "pathconfig.h"
#include <vtkPolyData.h>
#include <vtkPointData.h>
#include <vtkFloatArray.h>
#include <vtkIntArray.h>

#include <boost/program_options.hpp>
#include <boost/filesystem.hpp>
#include <cmath>
#include <vtkWarpScalar.h>
#include <vtkPlane.h>
#include <vtkCutter.h>
template<class writerType>
void writeGeometry( vtkDataObject *geom, const std::string fname )
{

    writerType *writer =  writerType::New();
    std::string fullname = fname + "." + writer->GetDefaultFileExtension();
    std::cout << "writing " << fullname << "....";
    writer->SetFileName( fullname.c_str() );
#if VTK_MAJOR_VERSION <= 5
    writer->SetInput( geom );
#else
    writer->SetInputData( geom );
#endif
    writer->SetDataModeToBinary();
    writer->Write();
    writer->Delete();
    std::cout << "done." << std::endl;
}

vtkSmartPointer<vtkPointSet> readPointSet( const boost::filesystem::path &pth )
{

    if ( pth.extension().string() == ".vtk" )
    {
        vtkSmartPointer<vtkUnstructuredGridReader> source_reader = vtkSmartPointer<vtkUnstructuredGridReader>::New();
        //input_reader->SetFileName( ( filePathBuilder::dataPath() + "post_gerris_output.vtk" ).c_str() );
        source_reader->SetFileName( pth.string().c_str() );
        source_reader->Update();
        //source_reader->GetOutput();
        return source_reader->GetOutput();

    }
    else if ( pth.extension().string() == ".vtu" )
    {
        vtkSmartPointer<vtkXMLUnstructuredGridReader> source_reader = vtkSmartPointer<vtkXMLUnstructuredGridReader>::New();
        source_reader->SetFileName( pth.string().c_str() );
        source_reader->Update();
        return source_reader->GetOutput();
    }

    //else //pth.extension().string() == ".vtp"

    vtkSmartPointer<vtkXMLPolyDataReader> source_reader = vtkSmartPointer<vtkXMLPolyDataReader>::New();
    source_reader->SetFileName( pth.string().c_str() );
    source_reader->Update();
    return source_reader->GetOutput();
    //else  //sfpath.extension().string() == ".vtu"

}


int main( int ac, char *av[] )
{

    namespace po = boost::program_options;
    namespace fs  = boost::filesystem;

    boost::filesystem::path ifpath; // input filename
    int polysNumber {0};
    po::options_description desc( "Options" );
    desc.add_options()
    ( "help,h", "Usage: extractPolys --input " )
    ( "input,i", po::value<std::string>(), "Input file" )
    ( "number,n", po::value<int>( &polysNumber ), "Number of polys to extract" )
    ;

    po::variables_map vm;
    po::store( po::parse_command_line( ac, av, desc ), vm );

    if ( vm.count( "help" ) )
    {
        std::cout << desc << "\n";
        return 0;
    }

    if ( vm.count( "input" ) )
    {
        boost::filesystem::path tmpf( vm["input"].as<std::string>() );

        if ( boost::filesystem::exists( tmpf ) && ( tmpf.extension().string() == ".vtk"  || tmpf.extension().string() == ".vtu" ) )
        {
            ifpath = tmpf;
        }
        else {std::cerr << "not 2dm file or input file doesn't exist" << std::endl; return 0;}
    }
    else
    {
        std::cout << "no input file given, aborting" << std::endl;
        return 0;
    }

    vtkSmartPointer<vtkPointSet> data = readPointSet( ifpath );

    vtkFloatArray *p_arr = vtkFloatArray::SafeDownCast( data->GetPointData()->GetArray( "P" ) );
    vtkFloatArray *u_arr = vtkFloatArray::SafeDownCast( data->GetPointData()->GetArray( "U" ) );
    vtkFloatArray *v_arr = vtkFloatArray::SafeDownCast( data->GetPointData()->GetArray( "V" ) );

    if ( p_arr && u_arr && v_arr ) {std::cout << "je!\n";}

    vtkSmartPointer<vtkIntArray> cc = vtkSmartPointer<vtkIntArray>::New();
    cc->SetNumberOfValues( data->GetNumberOfPoints() );
    cc->SetName( "ClausenClark" );
    double p {0.}, u {0.}, v {0.};
    double vel {0.};

    for ( unsigned int i = 0; i < data->GetNumberOfPoints(); i++ )
    {
        p = p_arr->GetValue( i );

        cc->SetValue( i, 0 ); // default
        if ( p > 0.01 )
        {
            u = u_arr->GetValue( i );
            v = v_arr->GetValue( i );
            vel = std::sqrt( u/p * u/p + v/p * v/p );
            cc->SetValue( i, 1 ); // default
            if ( vel > 2.0 )
            {
                if ( vel * p > 3 )
                { cc->SetValue( i, 2 ); } // partial damage

                if ( vel * p > 7 )
                { cc->SetValue( i, 3 ); } // total destruction
            }
        }
    }

    data->GetPointData()->SetScalars( cc );

    writeGeometry<vtkXMLUnstructuredGridWriter>( data, "/data/projekti/rjecina/simulacija_rjecina/new_frict/mreza_63/cc" );


    int arc_length_id = data->GetPointData()->SetActiveAttribute( "ClausenClark", vtkDataSetAttributes::SCALARS );

    vtkSmartPointer<vtkWarpScalar> warpScalar =  vtkSmartPointer<vtkWarpScalar>::New();
    warpScalar->SetInputData( data );
    warpScalar->SetScaleFactor( 1 ); // use the scalars themselves
    warpScalar->Update();

    writeGeometry<vtkXMLUnstructuredGridWriter>( warpScalar->GetOutput(), "/data/projekti/rjecina/simulacija_rjecina/new_frict/mreza_63/ws" );

    double bounds[6];
    warpScalar->GetOutput()->GetBounds( bounds );
    std::cout << "Bounds: "
              << bounds[0] << ", " << bounds[1] << " "
              << bounds[2] << ", " << bounds[3] << " "
              << bounds[4] << ", " << bounds[5] << std::endl;

    vtkSmartPointer<vtkPlane> plane = vtkSmartPointer<vtkPlane>::New();
    plane->SetOrigin( ( bounds[1] + bounds[0] ) / 2.0,
                      ( bounds[3] + bounds[2] ) / 2.0,
                      bounds[4] );
    plane->SetNormal( 0, 0, 1 );

    // Create cutter
    double high = plane->EvaluateFunction( ( bounds[1] + bounds[0] ) / 2.0,
                                           ( bounds[3] + bounds[2] ) / 2.0,
                                           bounds[5] );

    vtkSmartPointer<vtkCutter> cutter =
        vtkSmartPointer<vtkCutter>::New();
    cutter->SetInputConnection( warpScalar->GetOutputPort() );
    cutter->SetCutFunction( plane );
    cutter->SetValue( 0, 0.5 );
    cutter->SetValue( 1, 1.5 );
    cutter->SetValue( 2, 2.5 );
    cutter->Update();

    writeGeometry<vtkXMLPolyDataWriter>( cutter->GetOutput(), "/data/projekti/rjecina/simulacija_rjecina/new_frict/mreza_63/wsc" );
    
    std::cout<<cutter->GetOutput()->GetNumberOfLines()<<std::endl;
    std::cout<<cutter->GetOutput()->GetNumberOfPolys()<<std::endl;
    
    /*
     * # get point coords from vtk
     * XYvtk = data.GetPoints().GetData()
     * XY = vtk_to_numpy(XYvtk)[:,:2]
     * TRANS = [5456492, 5019989]  # translation vector
     * XY = XY + TRANS
     */
    return 0;
}
