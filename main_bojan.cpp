#include <vtkVersion.h>
#include <vtkSmartPointer.h>
#include <vtkProperty.h>
#include <vtkPoints.h>
#include <vtkCellArray.h>
#include <vtkPolyData.h>
#include <vtkPointData.h>
#include <vtkDelaunay2D.h>
#include <vtkMath.h>
#include <vtkPolyDataMapper.h>
#include <vtkActor.h>
#include <vtkRenderWindow.h>
#include <vtkRenderer.h>
#include <vtkRenderWindowInteractor.h>
#include <vtkVertexGlyphFilter.h>

#include <vtkProbeFilter.h>
#include <vtkTransform.h>
#include <vtkTransformFilter.h>
#include <vtkElevationFilter.h>
#include <vtkWarpScalar.h>

#include <vtkSTLWriter.h>
#include <vtkXMLPolyDataWriter.h>
#include <vtkDEMReader.h>
#include <vtkImageDataGeometryFilter.h>

#include <vtkStructuredGrid.h>
#include <vtkXMLStructuredGridWriter.h>

#include "pathconfig.h"
#include <fstream>
#include <iostream>
#include <sstream>
#include <string>
#include <map>
#include <set>
#include <cctype>
using std::ifstream;

template<class writerType>
void writeGeometry(vtkDataObject* geom, const std::string fname)
{
    std::cout<<"writing "<<fname<<"....";
    writerType* writer =  writerType::New();
    writer->SetFileName(fname.c_str());
#if VTK_MAJOR_VERSION <= 5
    writer->SetInput(geom);
#else
    writer->SetInputData(geom);
#endif
    writer->SetDataModeToBinary();
    writer->Write();
    writer->Delete();
    std::cout<<"done."<<std::endl;
}

class node
{
public:
    node(double _x, double _y, double _z) : x(_x), y(_y), z(_z) {}
    double x,y,z;
};
class cell
{
public:
    cell() {}
    std::string type;

};
class tricell : public cell
{
public:
    tricell(int _id1, int _id2, int _id3) : id1(_id1), id2(_id2), id3(_id3) {};
    int id1, id2, id3;
};
class quadcell : public cell
{
public:
    quadcell(int _id1, int _id2, int _id3, int _id4) :
        id1(_id1), id2(_id2), id3(_id3), id4(_id4) {};
    int id1, id2, id3, id4;
};
class Reader2dm
{
public:
    Reader2dm(const std::string f)
    {
        points = vtkSmartPointer<vtkPoints>::New();
        polys = vtkSmartPointer<vtkCellArray>::New();
        ifstream fin(f.c_str());
        std::string str;
        int celids =0;
        int ptid = 0;
        while ( fin>>str )
        {

            if(str== "E3T")
            {
                ++celids;
                vtkIdType id, a, b, c, one;
                fin >> id >> a >> b >> c >> one;
                trimap.insert(std::pair<int,tricell>(id,tricell(a,b,c)));
            }
            else if (str== "E4Q")
            {
                vtkIdType id, a, b, c, d, one;
                fin >> id >> a >> b >> c >>d >> one;
                quadmap.insert(std::pair<int,quadcell>(id,quadcell(a,b,c,d)));
                ++celids;
            }
            if ( str=="ND" )
            {
                int id;
                double x,y,z;
                fin>>id;
                fin>>x;
                fin>>y;
                fin>>z;
                //if(z<65)
                //{
                points->InsertNextPoint(x, y, z);
                ptmap.insert(std::pair<int,node>(id,node(x,y,z)));
                node2ptmap.insert(std::pair<int,int>(id,ptid));
                ++ptid;
                //}
                //else {
                //    exclude_ptids.insert(id);
                //}
            }
        }
        for(auto& item : trimap)
        {
            /*if(exclude_ptids.find(item.second.id1)==exclude_ptids.end() &&
                    exclude_ptids.find(item.second.id2)==exclude_ptids.end() &&
                    exclude_ptids.find(item.second.id3)==exclude_ptids.end()
              )*/
            //{
            polys->InsertNextCell(3);
            polys->InsertCellPoint(node2ptmap[item.second.id1]);
            polys->InsertCellPoint(node2ptmap[item.second.id2]);
            polys->InsertCellPoint(node2ptmap[item.second.id3]);
            //}
        }
        for(auto& item : quadmap)
        {
            /*if(exclude_ptids.find(item.second.id1)==exclude_ptids.end() &&
                    exclude_ptids.find(item.second.id2)==exclude_ptids.end() &&
                    exclude_ptids.find(item.second.id3)==exclude_ptids.end() &&
                    exclude_ptids.find(item.second.id4)==exclude_ptids.end()
              )*/
            //{
            polys->InsertNextCell(4);
            polys->InsertCellPoint(node2ptmap[item.second.id1]);
            polys->InsertCellPoint(node2ptmap[item.second.id2]);
            polys->InsertCellPoint(node2ptmap[item.second.id3]);
            polys->InsertCellPoint(node2ptmap[item.second.id4]);
            //}
        }
    }
    vtkSmartPointer<vtkPoints> getPoints() const {
        return points;
    }
    vtkSmartPointer<vtkCellArray> getCells() const {
        return polys;
    }
    
private:
    vtkSmartPointer<vtkPoints> points;
    vtkSmartPointer<vtkCellArray> polys;
    std::set<int> exclude_ptids;
    std::map<int, node> ptmap;
    std::map<int, int> node2ptmap; // 1. je ptmap key, 2. je redni broj
    std::map<int, quadcell> quadmap;
    std::map<int, tricell> trimap;
};

class regularMesh
{
public:
    regularMesh(double bounds[6], vtkSmartPointer<vtkPolyData> polydata, const int nx, const int ny) :
        probePolyData(polydata), xmin(bounds[0]), xmax(bounds[1]), ymin(bounds[2]),ymax(bounds[3]),zmin(bounds[4]),zmax(bounds[5])
    {
	double xsafetydistance=0;//add some safety distance to the border of the domain
	double ysafetydistance=-1;//add some safety distance to the border of the domain
	xmin-=xsafetydistance;
	xmax+=xsafetydistance;
	ymin-=ysafetydistance;
	ymax+=ysafetydistance;
	
	//Ovo je bilo potrebno kod postavljanja maninga
// 	xmin=-22;
// 	xmax=1450;
// 	ymin=-779;
// 	ymax=1452;
    
        points = vtkSmartPointer<vtkPoints>::New();
        double dx, dy;
        dx = (xmax-xmin)/nx;
        dy = (ymax-ymin)/ny;
        std::cout<<"dx = "<<dx<<"\t dy = "<<dy<<std::endl;
        double x = xmin;
        double y = ymin;
        for(int j=0; j<=ny; ++j)
        {
            x=xmin;
            for(int i=0; i<=nx; ++i)
            {
                points->InsertNextPoint(x, y, 0.);
                x+=dx;
            }
            y+=dy;
        }

        structuredGrid = vtkSmartPointer<vtkStructuredGrid>::New();
        structuredGrid->SetDimensions(nx+1,ny+1,1);
        structuredGrid->SetPoints(points);

        vtkSmartPointer<vtkTransform> flattener = vtkSmartPointer<vtkTransform>::New();
        flattener->Scale(1.0,1.0,0.0);
        flattener->Update();

        vtkSmartPointer<vtkElevationFilter> s_elev = vtkSmartPointer<vtkElevationFilter>::New();
        s_elev->SetInput(polydata);
        s_elev->SetHighPoint(0,0,zmax);
        s_elev->SetLowPoint(0,0,zmin);
        s_elev->SetScalarRange(bounds[4],bounds[5]);

        writeGeometry<vtkXMLPolyDataWriter>(s_elev->GetPolyDataOutput(),filePathBuilder::resultsPath()+"elevations.vtp");

        vtkSmartPointer<vtkTransformFilter> s_flat = vtkSmartPointer<vtkTransformFilter>::New();
        s_flat->SetInput(s_elev->GetOutput());
        s_flat->SetTransform(flattener);

        vtkSmartPointer<vtkTransformFilter> i_flat = vtkSmartPointer<vtkTransformFilter>::New();
        i_flat->SetInput(structuredGrid);
        i_flat->SetTransform(flattener);

        writeGeometry<vtkXMLStructuredGridWriter>(i_flat->GetOutput(),filePathBuilder::resultsPath()+"i_flat.vts");

        vtkSmartPointer<vtkProbeFilter> probe = vtkSmartPointer<vtkProbeFilter>::New();
        std::cout<<"performing probe..."<<std::endl;
#if VTK_MAJOR_VERSION <= 5
	std::cout<<"performing SetSource..."<<std::endl;
        probe->SetSource(s_flat->GetOutput());
	std::cout<<"performing SetInput..."<<std::endl;
        probe->SetInput(i_flat->GetOutput());
#else
	std::cout<<"performing SetSourceData..."<<std::endl;
        probe->SetSourceData(structuredGrid);
	std::cout<<"performing SetInputData..."<<std::endl;
        probe->SetInputData(probePolyData);
#endif
	std::cout<<"performing probe->Update..."<<std::endl;
        probe->Update();

        vtkIdTypeArray* arr = probe->GetValidPoints();
        double pt_coo[3];
	
	
	//double tx=0;//x translation of coordinate system
	//double ty=0;//y translation of coordinate system
	
	//Translacija koordinatnog sustava u blizinu nule
	
	double tx=-5456491.999777;//x translation of coordinate system
	double ty=-5019989.003335;//y translation of coordinate system
	
        std::cout<<"setting unspecified values..."<<std::endl;	
        for (vtkIdType pt_id = 0; pt_id<probe->GetOutput()->GetNumberOfPoints(); ++pt_id)
        {
            if(arr->LookupValue(pt_id)== -1) // valid point not found, should set scalar to -90
            {
		//std::cout<<"-1 "<<pt_coo[0]<<","<<pt_coo[1]<<std::endl;
                 probe->GetOutput()->GetPoint(pt_id,pt_coo);
		//Neumrezeni rubovi dobiju posebnu visinu
                if((pt_coo[0]+tx< -790 && pt_coo[1]+ty< 720) || (pt_coo[0]+tx> 1400 && pt_coo[1]+ty< -290) )
                {
                    probe->GetOutput()->GetPointData()->GetScalars()->SetTuple1(pt_id,-10);
                }
                else if ((pt_coo[0]+tx> 0 && pt_coo[0]+tx< 1400) && pt_coo[1]+ty< -153)
                {
                    probe->GetOutput()->GetPointData()->GetScalars()->SetTuple1(pt_id,-10);
		    //std::cout<<"usao "<<pt_coo[0]<<","<<pt_coo[1]<<std::endl;
                }
                else
                {
                    probe->GetOutput()->GetPointData()->GetScalars()->SetTuple1(pt_id,30);
                }
		
                //probe->GetOutput()->GetPointData()->GetScalars()->SetTuple1(pt_id,0.02); // ovo postavljanje manninga
		//probe->GetOutput()->GetPoint(pt_id,pt_coo);
		//std::cout<<"-1 "<<pt_coo[0]<<","<<pt_coo[1]<<std::endl;
            }
            //std::cout<<"1 "<<pt_coo[0]<<","<<pt_coo[1]<<std::endl;
        }
        std::cout<<"\tdone..."<<std::endl;
        writeGeometry<vtkXMLStructuredGridWriter>(probe->GetOutput(),filePathBuilder::resultsPath()+"probe.vts");

        gridWarpScalar = vtkSmartPointer<vtkWarpScalar>::New();
        gridWarpScalar->SetInputConnection(probe->GetOutputPort());
        gridWarpScalar->Update();
        resGeomtry = gridWarpScalar->GetOutput();
        writeGeometry<vtkXMLStructuredGridWriter>(gridWarpScalar->GetOutput(),filePathBuilder::resultsPath()+"regular_mesh.vts");
	std::cout<<"move mesh..."<<std::endl;	
	move(tx,ty);//translation of the coordinate system
    }
    void move(double tx, double ty)
    {
        vtkDataArray* arr = resGeomtry->GetPoints()->GetData();
        double coo[3];
	for(vtkIdType i =0; i<arr->GetNumberOfTuples(); ++i)
        {
            arr->GetTuple(i, coo);
	    coo[0]=coo[0]+tx;
	    coo[1]=coo[1]+ty;
	    //std::cout<<"moved "<<coo[0]<<","<<coo[1]<<","<<coo[2]<<std::endl;
	    arr->SetTuple(i,coo);
        }
    }
    void exportToXYZ(const std::string f_name)
    {
        //filePathBuilder dataloader;
        double bnds[6];
        resGeomtry->GetPoints()->GetBounds(bnds);
        std::cout<<"bounds: "<< bnds[0]<<" : "<<bnds[1]<<" : "<<bnds[2]<<" : "<<
                 bnds[3]<<" : "<<bnds[4]<<" : "<<bnds[5]<<std::endl;
        ofstream of(f_name.c_str());
        vtkDataArray* arr = resGeomtry->GetPoints()->GetData();
        double coo[3];
        for(vtkIdType i =0; i<arr->GetNumberOfTuples(); ++i)
        {
            arr->GetTuple(i, coo);
            if(coo[2]>-90)
            {
                of<<coo[0]<<" "<<coo[1]<<" "<<coo[2]<<std::endl;
            }
        }
        of.close();
    }
private:
    vtkSmartPointer<vtkPoints> points;
    vtkSmartPointer<vtkStructuredGrid> structuredGrid;
    vtkSmartPointer<vtkPolyData> probePolyData;
    vtkSmartPointer<vtkWarpScalar> gridWarpScalar;
    vtkPointSet* resGeomtry;
    double xmin, xmax, ymin, ymax, zmin, zmax;
};

int main(int, char *[])
{
    int grid_size=2000;
    //std::string name = /*"manning";*/"MrtviKanal_04_novageodezija_rjecina_2";
    //std::string name = /*"manning";*/"MrtviKanal_Jerko_ulaznikanal";
    std::string name="MrtviKanal_30";
    //std::string name="MrtviKanal_20_manning";
    //std::string name = "manning";
    //std::string s_grid_size = std::to_string(grid_size);

    //filePathBuilder dataloader;
    Reader2dm r(filePathBuilder::dataPath()+name+".2dm");
    
    // Add the grid points to a polydata object
    vtkSmartPointer<vtkPolyData> polydata = vtkSmartPointer<vtkPolyData>::New();
    polydata->SetPoints(r.getPoints());
    polydata->SetPolys(r.getCells());
    writeGeometry<vtkXMLPolyDataWriter>(polydata,filePathBuilder::resultsPath()+name +".vtp");

    double bnds[6];
    polydata->GetBounds(bnds);
    std::cout<<"bounds: "<< bnds[0]<<" : "<<bnds[1]<<" : "<<bnds[2]<<" : "<<
             bnds[3]<<" : "<<bnds[4]<<" : "<<bnds[5]<<std::endl;
    
    regularMesh rm(bnds, polydata, grid_size, grid_size);

    rm.exportToXYZ(filePathBuilder::resultsPath()+name +".xyz");


    return EXIT_SUCCESS;
}
