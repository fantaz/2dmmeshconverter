#include <vtkVersion.h>
#include "2dmReader.h"
#include <vtkPolyData.h>
#include <vtkPointData.h>

#include <vtkProbeFilter.h>
#include <vtkTransform.h>
#include <vtkTransformFilter.h>
#include <vtkElevationFilter.h>
#include <vtkWarpScalar.h>

#include <vtkXMLPolyDataWriter.h>

#include <vtkStructuredGrid.h>
#include <vtkXMLStructuredGridWriter.h>
#include <vtkXMLStructuredGridReader.h>
#include <vtkXMLPolyDataReader.h>
#include <vtkSimplePointsWriter.h>
#include <vtkCellLocator.h>
#include <vtkDoubleArray.h>
#include "pathconfig.h"
#include <fstream>
#include <iostream>
#include <sstream>
#include <cctype>
#include <chrono>
using std::ifstream;

template<class writerType>
void writeGeometry ( vtkDataObject* geom, const std::string fname )
{
    std::cout<<"writing "<<fname<<"....";
    writerType* writer =  writerType::New();
    writer->SetFileName ( fname.c_str() );
#if VTK_MAJOR_VERSION <= 5
    writer->SetInput ( geom );
#else
    writer->SetInputData ( geom );
#endif
    writer->SetDataModeToBinary();
    writer->Write();
    writer->Delete();
    std::cout<<"done."<<std::endl;
}





class regularMesh
{
public:
    regularMesh ( double bounds[6], vtkSmartPointer<vtkPolyData> polydata, const int nx, const int ny ) :
        probePolyData ( polydata ), xmin ( bounds[0] ), xmax ( bounds[1] ), ymin ( bounds[2] ),ymax ( bounds[3] ),zmin ( bounds[4] ),zmax ( bounds[5] )
    {
        points = vtkSmartPointer<vtkPoints>::New();
        double dx, dy;
        dx = ( xmax-xmin ) /nx;
        dy = ( ymax-ymin ) /ny;
        std::cout<<"dx = "<<dx<<"\t dy = "<<dy<<std::endl;
        double x = xmin;
        double y = ymin;

        for ( int j=0; j<=ny; ++j )
        {
            x=xmin;

            for ( int i=0; i<=nx; ++i )
            {
                points->InsertNextPoint ( x, y, 0. );
                x+=dx;
            }

            y+=dy;
        }

        structuredGrid = vtkSmartPointer<vtkStructuredGrid>::New();
        structuredGrid->SetDimensions ( nx+1,ny+1,1 );
        structuredGrid->SetPoints ( points );

        vtkSmartPointer<vtkTransform> flattener = vtkSmartPointer<vtkTransform>::New();
        flattener->Scale ( 1.0,1.0,0.0 );
        flattener->Update();

        vtkSmartPointer<vtkElevationFilter> s_elev = vtkSmartPointer<vtkElevationFilter>::New();
        s_elev->SetInputData ( polydata );
        s_elev->SetHighPoint ( 0,0,zmax );
        s_elev->SetLowPoint ( 0,0,zmin );
        s_elev->SetScalarRange ( bounds[4],bounds[5] );
        s_elev->Update();

        writeGeometry<vtkXMLPolyDataWriter> ( s_elev->GetPolyDataOutput(),filePathBuilder::resultsPath() +"elevations.vtp" );

        vtkSmartPointer<vtkTransformFilter> s_flat = vtkSmartPointer<vtkTransformFilter>::New();
        //s_flat->SetInput(s_elev->GetOutput());
        s_flat->SetInputConnection ( s_elev->GetOutputPort() );
        s_flat->SetTransform ( flattener );
        s_flat->Update();

        vtkSmartPointer<vtkTransformFilter> i_flat = vtkSmartPointer<vtkTransformFilter>::New();
        //i_flat->SetInput(structuredGrid);
        i_flat->SetInputData ( structuredGrid );
        i_flat->SetTransform ( flattener );
        i_flat->Update();
        writeGeometry<vtkXMLStructuredGridWriter> ( i_flat->GetOutput(),filePathBuilder::resultsPath() +"i_flat.vts" );

        vtkSmartPointer<vtkProbeFilter> probe = vtkSmartPointer<vtkProbeFilter>::New();
        std::cout<<"performing probe..."<<std::endl;
        std::chrono::time_point<std::chrono::system_clock> start, end;
        start = std::chrono::system_clock::now();
#if VTK_MAJOR_VERSION <= 5
        probe->SetSource ( s_flat->GetOutput() );
        probe->SetInput ( i_flat->GetOutput() );
#else
        probe->SetSourceData ( s_flat->GetOutput() );
        probe->SetInputData ( i_flat->GetOutput() );
#endif
        probe->Update();
        end = std::chrono::system_clock::now();
        std::chrono::duration<double> elapsed_time = end-start;
        std::cout<<"vtk probe: "<<std::chrono::duration_cast<std::chrono::milliseconds> ( elapsed_time ).count() <<std::endl;
        
        //interpolate_with_celllocator();
        
        vtkIdTypeArray* arr = probe->GetValidPoints();
        double pt_coo[3];

        // translacija koo sustava blizu nule
        double tx=-5456491.999777;//x translation of coordinate system
        double ty=-5019989.003335;//y translation of coordinate system
        start = std::chrono::system_clock::now();

        for ( vtkIdType pt_id = 0; pt_id<probe->GetOutput()->GetNumberOfPoints(); ++pt_id )
        {
            if ( arr->LookupValue ( pt_id ) == -1 ) // valid point not found, should set scalar to -90
            {
                //std::cout<<"-1 "<<pt_coo[0]<<","<<pt_coo[1]<<std::endl;
                probe->GetOutput()->GetPoint ( pt_id,pt_coo );

                //Neumrezeni rubovi dobiju posebnu visinu
                if ( ( pt_coo[0]+tx< -790 && pt_coo[1]+ty< 720 ) || ( pt_coo[0]+tx> 1400 && pt_coo[1]+ty< -290 ) )
                {
                    probe->GetOutput()->GetPointData()->GetScalars()->SetTuple1 ( pt_id,-10 );
                }
                else if ( ( pt_coo[0]+tx> 0 && pt_coo[0]+tx< 1400 ) && pt_coo[1]+ty< -153 )
                {
                    probe->GetOutput()->GetPointData()->GetScalars()->SetTuple1 ( pt_id,-10 );
                    //std::cout<<"usao "<<pt_coo[0]<<","<<pt_coo[1]<<std::endl;
                }
                else
                {
                    probe->GetOutput()->GetPointData()->GetScalars()->SetTuple1 ( pt_id,30 );
                }

                //probe->GetOutput()->GetPointData()->GetScalars()->SetTuple1(pt_id,0.02); // ovo postavljanje manninga
                //probe->GetOutput()->GetPoint(pt_id,pt_coo);
                //std::cout<<"-1 "<<pt_coo[0]<<","<<pt_coo[1]<<std::endl;
            }

            //std::cout<<"1 "<<pt_coo[0]<<","<<pt_coo[1]<<std::endl;
        }

        std::cout<<"\tdone..."<<std::endl;
        end = std::chrono::system_clock::now();
        elapsed_time = end-start;
        std::cout<<"for if petlja probe: "<<std::chrono::duration_cast<std::chrono::milliseconds> ( elapsed_time ).count() <<std::endl;
        writeGeometry<vtkXMLStructuredGridWriter> ( probe->GetOutput(),filePathBuilder::resultsPath() +"probe.vts" );

        gridWarpScalar = vtkSmartPointer<vtkWarpScalar>::New();
        gridWarpScalar->SetInputConnection ( probe->GetOutputPort() );
        gridWarpScalar->Update();
        resGeomtry = gridWarpScalar->GetOutput();
        writeGeometry<vtkXMLStructuredGridWriter> ( gridWarpScalar->GetOutput(),filePathBuilder::resultsPath() +"regular_mesh.vts" );
        std::cout<<"move mesh...";
        move ( tx,ty ); //translation of the coordinate system
        std::cout<<"\tdone..."<<std::endl;
    }
    void interpolate_with_celllocator()
    {
        std::cout<<"eto i mene"<<std::endl;
        vtkSmartPointer<vtkElevationFilter> s_elev = vtkSmartPointer<vtkElevationFilter>::New();
        s_elev->SetInputData ( probePolyData );
        s_elev->SetHighPoint ( 0,0,zmax );
        s_elev->SetLowPoint ( 0,0,zmin );
        s_elev->SetScalarRange ( zmin,zmax );
        s_elev->Update();

        vtkSmartPointer<vtkTransform> flattener = vtkSmartPointer<vtkTransform>::New();
        flattener->Scale ( 1.0,1.0,0.0 );
        flattener->Update();
        
        vtkSmartPointer<vtkTransformFilter> s_flat = vtkSmartPointer<vtkTransformFilter>::New();
        //s_flat->SetInput(s_elev->GetOutput());
        s_flat->SetInputConnection ( s_elev->GetOutputPort() );
        s_flat->SetTransform ( flattener );
        s_flat->Update();

        vtkSmartPointer<vtkTransformFilter> i_flat = vtkSmartPointer<vtkTransformFilter>::New();
        //i_flat->SetInput(structuredGrid);
        i_flat->SetInputData ( structuredGrid );
        i_flat->SetTransform ( flattener );
        i_flat->Update();
        
        // Now find the elevation with a CellLocator
        vtkSmartPointer<vtkCellLocator> cellLocator = vtkSmartPointer<vtkCellLocator>::New();
        cellLocator->SetDataSet ( s_elev->GetOutput() );
        cellLocator->BuildLocator();
        cellLocator->Update();
        std::cout<<"eto i mene"<<std::endl;
        vtkDataArray* arrcoords = structuredGrid->GetPoints()->GetData();
        std::cout<<"eto i mene"<<std::endl;
        std::chrono::time_point<std::chrono::system_clock> start, end;
        int subId;
        double t, xyz[3], pcoords[3];
        double rayStart[3], rayEnd[3];
        double tx=-5456491.999777;//x translation of coordinate system
        double ty=-5019989.003335;//y translation of coordinate system
        start = std::chrono::system_clock::now();
        for ( int i = 0; i < structuredGrid->GetNumberOfPoints(); i++ )
        {

            structuredGrid->GetPoint ( i, rayStart );
            //std::cout<<"eto i mene"<<std::endl;
            rayStart[2] += 1000.0;
            structuredGrid->GetPoint ( i, rayEnd );
            rayEnd[2] -= 1000.0;

            if ( cellLocator->IntersectWithLine (
                        rayStart,
                        rayEnd,
                        0.0001,
                        t,
                        xyz,
                        pcoords,
                        subId ) )
            {
                //cout << "Interpolation using CellLocator ";
                //cout << "Elevation at " << rayStart[0] << ", " << rayStart[1] << " is " << xyz[2] << endl;
                rayStart[2] = xyz[2];
                arrcoords->SetTuple ( i,rayStart );
            }
            else
            {
                //Neumrezeni rubovi dobiju posebnu visinu
                if ( ( rayStart[0]+tx< -790 && rayStart[1]+ty< 720 ) || 
                    ( rayStart[0]+tx> 1400 && rayStart[1]+ty< -290 ) )
                {
                    rayStart[2] = -10;
                    arrcoords->SetTuple ( i,rayStart );
                }
                else if ( ( rayStart[0]+tx> 0 && rayStart[0]+tx< 1400 ) && rayStart[1]+ty< -153 )
                {
                    rayStart[2] = -10;
                    arrcoords->SetTuple ( i,rayStart );
                }
                else
                {
                    rayStart[2] = 30;
                    arrcoords->SetTuple ( i,rayStart );
                }
                
            }
        }
        end = std::chrono::system_clock::now();
        std::chrono::duration<double> elapsed_time = end-start;
        std::cout<<"cell locator: "<<std::chrono::duration_cast<std::chrono::milliseconds> ( elapsed_time ).count() <<std::endl;
        writeGeometry<vtkXMLStructuredGridWriter> ( structuredGrid,filePathBuilder::resultsPath() +"strgrid_flat.vts" );
    }
    void move ( const double &tx, const double &ty )
    {
        vtkDataArray* arr = resGeomtry->GetPoints()->GetData();
        double coo[3];

        for ( vtkIdType i =0; i<arr->GetNumberOfTuples(); ++i )
        {
            arr->GetTuple ( i, coo );
            coo[0]=coo[0]+tx;
            coo[1]=coo[1]+ty;
            arr->SetTuple ( i,coo );
        }
    }
    void exportToXYZ ( const std::string f_name )
    {
        //filePathBuilder dataloader;
        double bnds[6];
        resGeomtry->GetPoints()->GetBounds ( bnds );
        std::cout<<"bounds: "<< bnds[0]<<" : "<<bnds[1]<<" : "<<bnds[2]<<" : "<<
                 bnds[3]<<" : "<<bnds[4]<<" : "<<bnds[5]<<std::endl;
        ofstream of ( f_name.c_str() );
        vtkDataArray* arr = resGeomtry->GetPoints()->GetData();
        double coo[3];

        for ( vtkIdType i =0; i<arr->GetNumberOfTuples(); ++i )
        {
            arr->GetTuple ( i, coo );

            if ( coo[2]>-90 )
            { of<<coo[0]<<" "<<coo[1]<<" "<<coo[2]<<std::endl; }
        }

        of.close();
    }
private:
    vtkSmartPointer<vtkPoints> points;
    vtkSmartPointer<vtkStructuredGrid> structuredGrid;
    vtkSmartPointer<vtkPolyData> probePolyData;
    vtkSmartPointer<vtkWarpScalar> gridWarpScalar;
    vtkPointSet* resGeomtry;
    double xmin, xmax, ymin, ymax, zmin, zmax;
};

//! \brief ako vec imamo regularnu mrezu i treba napraviti samo xyz
class xyzexporter_helper
{
public:
    xyzexporter_helper ( const std::string &fname ) : datasetName ( fname )
    {
        std::cout<<"reading from "<<datasetName<<"...";
        vtkSmartPointer<vtkXMLStructuredGridReader> reader = vtkSmartPointer<vtkXMLStructuredGridReader>::New();
        reader->SetFileName ( datasetName.c_str() );
        reader->Update();
        sgrid = vtkSmartPointer<vtkStructuredGrid>::New();
        sgrid->DeepCopy ( reader->GetOutput() );
        std::cout<<" done!"<<std::endl;

    }
    void exportToXYZ ( const std::string &f_name )
    {
        //writeGeometry<vtkXMLStructuredGridWriter> ( sgrid,filePathBuilder::resultsPath() +"moja_proba" +".vts" );


        std::cout<<"moving mesh...";
        vtkSmartPointer<vtkTransform> transform = vtkSmartPointer<vtkTransform>::New();

        // translacija koo sustava blizu nule
        const double tx=-5456491.999777;//x translation of coordinate system
        const double ty=-5019989.003335;//y translation of coordinate system
        transform->Translate ( tx,ty,0. );
        vtkSmartPointer<vtkTransformFilter> transformFilter = vtkSmartPointer<vtkTransformFilter>::New();
        transformFilter->SetInputData ( sgrid );
        transformFilter->SetTransform ( transform );
        transformFilter->Update();
        std::cout<<" done"<<std::endl;
        std::cout<<"writing to xyz...";
        /*
        ofstream of ( f_name.c_str() );
        vtkDataArray* arr = transformFilter->GetOutput()->GetPoints()->GetData();
        double coo[3];
        for ( vtkIdType i =0; i<arr->GetNumberOfTuples(); ++i )
        {
            arr->GetTuple ( i, coo );
            if ( coo[2]>-90 )
            { of<<coo[0]<<" "<<coo[1]<<" "<<coo[2]<<std::endl; }
        }
        of.close();*/
        vtkSmartPointer<vtkSimplePointsWriter> writer = vtkSmartPointer<vtkSimplePointsWriter>::New();
        writer->SetFileName ( f_name.c_str() );
        writer->SetInputConnection ( transformFilter->GetOutputPort() );
        writer->Write();
        std::cout<<"done"<<std::endl;
        std::cout<<"exporting vts for fun...";
        writeGeometry<vtkXMLStructuredGridWriter> ( transformFilter->GetOutput(),filePathBuilder::resultsPath() +"moja_proba" +".vts" );
        std::cout<<" done!"<<std::endl;
    }
private:
    std::string datasetName;
    vtkSmartPointer<vtkStructuredGrid> sgrid;
};
#define VTK_CREATE(type, name) \
    vtkSmartPointer<type> name = vtkSmartPointer<type>::New();
int main ( int, char *[] )
{
//     std::string name = /*"manning";*/"MrtviKanal_37_refinezid_03";
//     int grid_size=100;
//     Reader2dm r ( filePathBuilder::dataPath() +name +".2dm" );
//
//     // Add the grid points to a polydata object
//     vtkSmartPointer<vtkPolyData> polydata = vtkSmartPointer<vtkPolyData>::New();
//     polydata->SetPoints ( r.getPoints() );
//     polydata->SetPolys ( r.getCells() );
//
//     writeGeometry<vtkXMLPolyDataWriter> ( polydata,filePathBuilder::resultsPath() +name +".vtp" );
//     r.writeNodestringsToFile ( filePathBuilder::resultsPath() );
//
//     double bnds[6];
//     polydata->GetBounds ( bnds );
//     std::cout<<"bounds: "<< bnds[0]<<" : "<<bnds[1]<<" : "<<bnds[2]<<" : "<<
//              bnds[3]<<" : "<<bnds[4]<<" : "<<bnds[5]<<std::endl;
//
//
//     regularMesh rm ( bnds, polydata, grid_size, grid_size );
//     rm.exportToXYZ ( filePathBuilder::resultsPath() +name +".xyz" );

    //xyzexporter_helper helper(filePathBuilder::resultsPath()+"mrtvikanal_37_refinezid_03/MrtviKanal_37_refinezid_03_regular_mesh.vts");
    //helper.exportToXYZ(filePathBuilder::resultsPath() +"MrtviKanal_37_refinezid_03_regular_mesh.xyz");
    VTK_CREATE ( vtkXMLPolyDataReader, reader );
    std::string vtp_name = "MrtviKanal_37_refinezid_03_3";
    reader->SetFileName ( ( filePathBuilder::resultsPath() +vtp_name+".vtp" ).c_str() );
    reader->Update();

    double bnds[6];
    reader->GetOutput()->GetBounds ( bnds );
    std::cout<<"bounds: "<< bnds[0]<<" : "<<bnds[1]<<" : "<<bnds[2]<<" : "<<
             bnds[3]<<" : "<<bnds[4]<<" : "<<bnds[5]<<std::endl;
    int grid_size=200;
    regularMesh rm ( bnds, reader->GetOutput(), grid_size, grid_size );
    //rm.exportToXYZ ( filePathBuilder::resultsPath() +name +".xyz" );
    return EXIT_SUCCESS;
}
