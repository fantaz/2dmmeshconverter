#ifndef _interpolator_h
#define _intepolator_h 
#include <vtkSmartPointer.h>
#include <vtkDataArray.h>
#include <vtkDataSet.h>
#include <vtkCellLocator.h>
#include <sstream>
#include <functional>
// interpoliraju se z vrijednosti
// Potrebno je namjestiti z vrijednosti na mrezi arrcoords preko sflat
// Ako je tocka u arrcords izvan podrucja dataseta sflat, onda je potrebno nesto 
// smisliti. U tu svrhu se koristi std function koja se izvana zadaje i stavlja u 
// extrapolation_criterium.
// Pretpostavka da je arrcoords structured data set.
class Interpolator
{
private:
    vtkDataArray* arrcoords;
    const int numberOfThreads;
    vtkSmartPointer<vtkCellLocator> cellLocator;
    vtkDataSet* sflat;
    std::function<void(vtkDataArray* arr, double (& pt) [3], const int& cellid)> extrapolation_criterium;
public: 
    Interpolator(vtkDataArray* _arrcoords, vtkDataSet* _sf, 
        std::function<void(vtkDataArray* arr, double (& pt) [3], const int& cellid)> ex_fun,
        const int nthreads) : 
                            arrcoords(_arrcoords), 
                            numberOfThreads(nthreads),
                            sflat(_sf),
                            extrapolation_criterium(ex_fun)
    {
        cellLocator = vtkSmartPointer<vtkCellLocator>::New();
        cellLocator->SetDataSet ( sflat );
        cellLocator->BuildLocator();
        cellLocator->Update();
    }
    void doInterpolate ( const int &threadId )
    {
        int elements = arrcoords->GetNumberOfTuples();
        int sz = elements/numberOfThreads;
        int b = sz*threadId;
        int e = b+sz-1;

        if ( threadId == numberOfThreads-1 ) {e = elements-1;}
        //std::cout<<"elements = "<<elements<<std::endl;
        //std::cout<<"thread = "<<threadId<<"\t"<<b<<":"<<e<<"\t sz= "<<e-b+1<<std::endl;
        //std::cout<<"\tstarting thread id "<<threadId<<"with point range "<<b<<":"<<e<<std::endl<<std::flush;
        {std::stringstream stream;
        stream <<"\tstarting thread id "<<threadId<<" with point range: "<<b<<":"<<e<<std::endl;
        std::cout << stream.str()<<std::flush;}
        int subId;
        double t, xyz[3], pcoords[3];
        double rayStart[3], rayEnd[3];

        for ( int i=b; i<=e; ++i )
        {
            arrcoords->GetTuple(i,rayStart);
            rayStart[2] += 1000.0;
            arrcoords->GetTuple(i,rayEnd);
            rayEnd[2] -= 1000.0;

            if ( cellLocator->IntersectWithLine (
                        rayStart,
                        rayEnd,
                        0.0001,
                        t,
                        xyz,
                        pcoords,
                        subId ) )
            {
                rayStart[2] = xyz[2];
                arrcoords->SetTuple ( i,rayStart );
            }
            else
            {
                extrapolation_criterium(arrcoords,rayStart,i);
            }
        }
        {std::stringstream stream;
        //std::cout<<"\tdone with thread id: "<<threadId<<std::endl<<std::flush;
        stream<<"\tdone with thread id: "<<threadId<<std::endl;
        std::cout<<stream.str()<<std::flush;}
    }
    
};

#endif //_intepolator_h 
