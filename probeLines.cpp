#include <iostream>
#include <algorithm>
#include <string>
#include <map>
#include "pathconfig.h"
#include "charter.h"


#include <vtkUnstructuredGridReader.h>
#include <vtkProbeFilter.h>
#include <vtkSmartPointer.h>
#include <vtkLineSource.h>
#include <vtkDataSet.h>
#include <vtkPolyData.h>
#include <vtkDoubleArray.h>
#include <vtkFloatArray.h>
#include <vtkCellArray.h>
#include <vtkMath.h>
#include <vtkPointData.h>

#include <vtkXMLPolyDataWriter.h>
#include <boost/concept_check.hpp>
#include <boost/program_options.hpp>
#include <boost/filesystem.hpp>



namespace po = boost::program_options;

template<class writerType>
void writeGeometry( vtkDataObject *geom, const std::string fname )
{

    writerType *writer =  writerType::New();
    std::string fullname = fname + "." + writer->GetDefaultFileExtension();
    std::cout << "writing " << fullname << "....";
    writer->SetFileName( fullname.c_str() );
#if VTK_MAJOR_VERSION <= 5
    writer->SetInput( geom );
#else
    writer->SetInputData( geom );
#endif
    writer->SetDataModeToBinary();
    writer->Write();
    writer->Delete();
    std::cout << "done." << std::endl;
}

class AttributesOverLine
{
public:
    AttributesOverLine( const std::string &ifile, const int &lineresolution = 100 ) : inputfname( ifile ), line( nullptr ), line_resolution( lineresolution )
    {
        reader = vtkSmartPointer<vtkUnstructuredGridReader>::New();
        reader->SetFileName( inputfname.c_str() );
        reader->Update();
    }
    void extractOverLine( const double &x1, const double &y1, const double &x2, const double &y2 )
    {
        if ( line ) { line->Delete(); }

        vtkSmartPointer<vtkLineSource> probeLine = vtkLineSource::New();
        probeLine->SetPoint1( x1, y1, 0 );
        probeLine->SetPoint2( x2, y2, 0 );
        probeLine->SetResolution( line_resolution );

        vtkSmartPointer<vtkProbeFilter> probe = vtkSmartPointer<vtkProbeFilter>::New();
        probe->SetSourceConnection( reader->GetOutputPort() );
        probe->SetInputConnection( probeLine->GetOutputPort() );
        probe->Update();

        computeArcLength( probe->GetOutput() );
        double orthovec[2];
        std::tie( orthovec[0], orthovec[1] ) = computePerpendicural( probeLine.GetPointer() );
        computeProjectedVectors( probe->GetOutput(), "U", "V", orthovec, "projected_depth_velocity_2" );
        computeRealVelocities( probe->GetOutput() );
        computeProjectedVectors( probe->GetOutput(), "vx", "vy", orthovec, "projected_real_velocity" );
        integrals = integrateOverLine( probe->GetOutput() );
        stats = extractStats(probe->GetOutput());
        line = vtkPolyData::New();
        line->DeepCopy( probe->GetOutput() );
    }
    void saveLine2VTK( const std::string &fname )
    {
        //std::string fullname = filePathBuilder::resultsPath() + fname;
        //writeGeometry<vtkXMLPolyDataWriter>( line, fullname );
        boost::filesystem::path ifilep {inputfname};
        boost::filesystem::path parent_path {ifilep.parent_path()};
        parent_path /= fname;
        writeGeometry<vtkXMLPolyDataWriter>( line, parent_path.string() );
    }
    void printIntegrals( std::ostream &ostr )
    {
        ostr << "printing integrals for arrays:" << std::endl;

        for ( const auto & item : integrals )
        { ostr << item.first << " : " << item.second << std::endl; }
        ostr<<"-------------------------"<<std::endl;
    }
    
    void printStats( std::ostream &ostr )
    {
        ostr << "printing mean, min and max value for arrays:" << std::endl;

        for ( const auto & item : stats )
        { ostr << item.first << " : " << item.second.mean_val<< ", "<<item.second.range[0]<<", "<< item.second.range[1]<< std::endl; }
        ostr<<"-------------------------"<<std::endl;
    }
private:
    class Stats
    {
    public:
        double range[2], mean_val;
    };
    vtkSmartPointer<vtkUnstructuredGridReader> reader;
    std::string inputfname;
    std::map<std::string, double> integrals;
    std::map<std::string, Stats> stats;
    vtkPolyData *line;
    const int line_resolution;
private:
    //! \brief Compute the integral using the trapezoidal rule x-axis -> arc_length_arr, y-axis -> arr_to_integrate
    double computeTrapzIntegral( vtkDataArray *arc_length_arr, vtkDataArray *arr_to_integrate )
    {
        double sum {0};
        double val1 {0}, val2 {0};
        double x1 {0}, x2 {0};
        x1 = arc_length_arr->GetTuple1( 0 );
        val1 = arr_to_integrate->GetTuple1( 0 );

        for ( vtkIdType cc = 1; cc < arc_length_arr->GetNumberOfTuples(); cc++ )
        {
            x2 = arc_length_arr->GetTuple1( cc );
            val2 = arr_to_integrate->GetTuple1( cc );
            sum += 0.5 * ( x2 - x1 ) * ( val2 + val1 );
            x1 = x2;
            val1 = val2;
        }

        return sum;
    }
    //! \brief integrates all scalars over line and returns array of key-value pairs (scalar name, integral value)
    std::map<std::string, double> integrateOverLine( vtkDataSet *input )
    {
        vtkPolyData *pdata = vtkPolyData::SafeDownCast( input );
        int arc_length_id = pdata->GetPointData()->SetActiveAttribute( "arc_length", vtkDataSetAttributes::SCALARS );
        //std::cout << "index of arc_length scalars: " << arc_length_id << std::endl;
        vtkDataArray *arc_arr = pdata->GetPointData()->GetArray( arc_length_id ); // get arc_length

        // integrals of ALL scalars (scalar name, value of integral)
        std::map<std::string, double> scalar_integrals;

        for ( vtkIdType i = 0; i < pdata->GetPointData()->GetNumberOfArrays(); ++i )
        {
            // set active attribute
            int id = pdata->GetPointData()->SetActiveAttribute( i, vtkDataSetAttributes::SCALARS );

            if ( id != -1 )
            {
                vtkDataArray *arr = pdata->GetPointData()->GetAttribute( vtkDataSetAttributes::SCALARS ); // get active attribute
                // compute integral with trapz and put it in an array
                scalar_integrals.insert( std::make_pair( arr->GetName(), computeTrapzIntegral( arc_arr, arr ) ) );
            }
        }

        return scalar_integrals;
    }
    std::map<std::string, Stats> extractStats( vtkDataSet *input )
    {
        vtkPolyData *pdata = vtkPolyData::SafeDownCast( input );
        // stats of ALL scalars (scalar name, value of integral)
        std::map<std::string, Stats> scalar_stats;

        for ( vtkIdType i = 0; i < pdata->GetPointData()->GetNumberOfArrays(); ++i )
        {
            // set active attribute
            int id = pdata->GetPointData()->SetActiveAttribute( i, vtkDataSetAttributes::SCALARS );

            if ( id != -1 )
            {
                vtkDataArray *arr = pdata->GetPointData()->GetAttribute( vtkDataSetAttributes::SCALARS ); // get active attribute
                Stats s;
                s.mean_val=0.;
                
                for ( vtkIdType cc = 1; cc < arr->GetNumberOfTuples(); cc++ )
                {s.mean_val+=arr->GetTuple1(cc);}
                s.mean_val=s.mean_val/arr->GetNumberOfTuples();
                
                arr->GetRange(s.range);
                
                scalar_stats.insert( std::make_pair( arr->GetName(), s) );
            }
        }

        return scalar_stats;

    }
    //! \brief compute real vx,vy and add the resulting array to input dataset
    void computeRealVelocities( vtkDataSet *input )
    {
        vtkPolyData *pdata = vtkPolyData::SafeDownCast( input );
        vtkIdType numPoints = pdata->GetPoints()->GetNumberOfPoints();
        int br = pdata->GetPointData()->SetActiveAttribute( "U", vtkDataSetAttributes::SCALARS );
        //std::cout<<"br = "<<br<<std::endl;
        vtkDataArray *vx_arr = pdata->GetPointData()->GetArray( br );
        br = pdata->GetPointData()->SetActiveAttribute( "V", vtkDataSetAttributes::SCALARS );
        vtkDataArray *vy_arr = pdata->GetPointData()->GetArray( br );
        br = pdata->GetPointData()->SetActiveAttribute( "P", vtkDataSetAttributes::SCALARS );
        vtkDataArray *p_arr = pdata->GetPointData()->GetArray( br );

        vtkSmartPointer<vtkDoubleArray> realvx = vtkSmartPointer<vtkDoubleArray>::New();
        realvx->SetName( "vx" );
        realvx->SetNumberOfComponents( 1 );
        realvx->SetNumberOfTuples( numPoints );

        vtkSmartPointer<vtkDoubleArray> realvy = vtkSmartPointer<vtkDoubleArray>::New();
        realvy->SetName( "vy" );
        realvy->SetNumberOfComponents( 1 );
        realvy->SetNumberOfTuples( numPoints );
        double u, v, p;
        const double dry { 0.01 }; // if water depth is less than 3 cm, don't calculate velocities
        auto realvLambda = [&dry]( const double & _v, const double & _p )
        {
            if ( _p < dry ) { return 0.; }

            return _v / _p;
        };

        for ( vtkIdType cc = 0; cc < numPoints; cc++ )
        {
            u = vx_arr->GetTuple1( cc );
            v = vy_arr->GetTuple1( cc );
            p = p_arr->GetTuple1( cc );
            realvx->SetTuple1( cc, realvLambda( u, p ) );
            realvy->SetTuple1( cc, realvLambda( v, p ) );
        }

        pdata->GetPointData()->AddArray( realvx );
        pdata->GetPointData()->AddArray( realvy );

    }

    //! \brief computes projected vectors based on vecx_name and vecy_name components and adds the resulting array to input dataset
    void computeProjectedVectors( vtkDataSet *input, const std::string &vecx_name, const std::string &vecy_name, double proj_vec[], const std::string &newDataSetName )
    {
        vtkPolyData *pdata = vtkPolyData::SafeDownCast( input );
        vtkIdType numPoints = pdata->GetPoints()->GetNumberOfPoints();
        int br = pdata->GetPointData()->SetActiveAttribute( vecx_name.c_str(), vtkDataSetAttributes::SCALARS );
        vtkDataArray *vx_arr = pdata->GetPointData()->GetArray( br );
        br = pdata->GetPointData()->SetActiveAttribute( vecy_name.c_str(), vtkDataSetAttributes::SCALARS );
        vtkDataArray *vy_arr = pdata->GetPointData()->GetArray( br );
        vtkSmartPointer<vtkDoubleArray> pvelocity = vtkSmartPointer<vtkDoubleArray>::New();
        pvelocity->SetName( newDataSetName.c_str() );
        pvelocity->SetNumberOfComponents( 1 );
        pvelocity->SetNumberOfTuples( numPoints );
        double v[2];

        for ( vtkIdType cc = 0; cc < numPoints; cc++ )
        {
            v[0] = vx_arr->GetTuple1( cc );
            v[1] = vy_arr->GetTuple1( cc );
            pvelocity->SetTuple1( cc, v[0]*proj_vec[0] + v[1]*proj_vec[1] );
        }

        pdata->GetPointData()->AddArray( pvelocity );
    }

    //! \brief computes perpendicular line
    std::tuple<double, double> computePerpendicural( vtkLineSource *line )
    {
        double pt1[3], pt2[3], x[3];
        line->GetPoint1( pt1 );
        line->GetPoint2( pt2 );

        for ( size_t i = 0; i < 3; ++i )
        { x[i] = pt2[i] - pt1[i]; }

        vtkMath::Normalize( x );
        // okomica na liniju:
        double y[3];
        y[0] = -x[1];
        y[1] = x[0];
        return std::make_tuple( y[0], y[1] );
    }

    //! \brief computes the arc length and adds the resulting array to input dataset
    void computeArcLength( vtkDataSet *input )
    {

        vtkPolyData *pdata = vtkPolyData::SafeDownCast( input );
        vtkPoints *points = pdata->GetPoints();
        vtkIdType numPoints = points->GetNumberOfPoints();
        //std::cout<<"numpoints: "<<numPoints<<std::endl;

        vtkSmartPointer<vtkDoubleArray> arc_length = vtkSmartPointer<vtkDoubleArray>::New();
        arc_length->SetName( "arc_length" );
        arc_length->SetNumberOfComponents( 1 );
        arc_length->SetNumberOfTuples( numPoints );
        arc_length->FillComponent( 0, 0.0 );

        vtkCellArray *lines = pdata->GetLines();
        vtkIdType numCellPoints;
        vtkIdType *cellPoints;
        lines->InitTraversal();

        while ( lines->GetNextCell( numCellPoints, cellPoints ) )
        {
            if ( numCellPoints == 0 ) { continue; }

            double arc_distance = 0.0;
            double prevPoint[3];
            points->GetPoint( cellPoints[0], prevPoint );

            for ( vtkIdType cc = 1; cc < numCellPoints; cc++ )
            {
                double curPoint[3];
                points->GetPoint( cellPoints[cc], curPoint );
                double distance = sqrt( vtkMath::Distance2BetweenPoints( curPoint, prevPoint ) );
                arc_distance += distance;
                arc_length->SetTuple1( cellPoints[cc], arc_distance );

                memcpy( prevPoint, curPoint, 3 * sizeof( double ) );
                //std::copy(std::begin(curPoint), std::end(curPoint),std::begin(prevPoint));
            }
        }

        pdata->GetPointData()->AddArray( arc_length );
    }
};
int main( int argc, char **argv )
{
    po::options_description desc( "Allowed options" );
    desc.add_options()
    ( "help", "produce this help message\n Resulting integrals are dumped on std out." )
    ( "fname", po::value<std::string>(), "full path to vtk file" )
    ( "prefix", po::value<std::string>(), "prefix of generated files" )
    ( "resolution", po::value<int>(), "number of points to evaluate on the line" )
    ;

    po::variables_map vm;
    po::store( po::parse_command_line( argc, argv, desc ), vm );
    po::notify( vm );


    std::string ifile; //! input file
    std::string prefix {"std_prefix"}; //! prefix for generated files
    int line_resolution {100}; //! number of points to evaluate on line

    if ( vm.count( "help" ) )
    {
        cout << desc << "\n";
        return 0;
    }

    if ( vm.count( "prefix" ) )
    {
        cout << "prefix set to: " << vm["prefix"].as<std::string>() << "." << std::endl;
        prefix = vm["prefix"].as<std::string>();
    }
    else {cout << "prefix set to default value: " << prefix << "." << std::endl;}

    if ( vm.count( "fname" ) )
    {
        boost::filesystem::path existingPath( vm["fname"].as<std::string>() );

        if ( boost::filesystem::exists( existingPath ) && existingPath.extension().string() == ".vtk" )
        {
            ifile = existingPath.string();
            std::cout << "file to process: " << ifile << std::endl;
        }
        else
        {
            std::cout << vm["fname"].as<std::string>() << " file missing or not vtk file, aborting." << std::endl;
            return 0;
        }
    }
    else {cout << "fname not set, aborting:" << std::endl; return 0;}

    if ( vm.count( "resolution" ) )
    {
        cout << "resolution set to: " << vm["resolution"].as<int>() << "." << std::endl;
        line_resolution = vm["resolution"].as<int>();
    }
    else {cout << "line resolution set to default value: " << line_resolution << "." << std::endl;}


    //std::string prefix {"q360"};
    //std::string ifile = filePathBuilder::dataPath() + "q_200_c_simres-10000.00.vtk";
    AttributesOverLine aol( ifile, line_resolution );
    aol.extractOverLine( 413.877184824952, 193.624460351668, 452.818624006934, 173.467066676062 );
    aol.saveLine2VTK( prefix + "_mrtvi" );
    aol.printIntegrals( std::cout );
    aol.printStats( std::cout );
    std::cout<<"*******************"<<std::endl;
    
    //aol.extractOverLine( 623.511199358658, 499.805896726392, 665.596917932301, 497.242196081885 );
    aol.extractOverLine( 638.320611832825, 594.686940446823, 675.392569874638, 584.313167360407 );
    aol.saveLine2VTK( prefix + "_uzvodno" );
    aol.printIntegrals( std::cout );
    aol.printStats( std::cout );
    std::cout<<"*******************"<<std::endl;

    aol.extractOverLine( 651.50903722463, 155.33947533505, 691.896712854202, 173.603277574664 );
    aol.saveLine2VTK( prefix + "_nizvodno" );
    aol.printIntegrals( std::cout );
    aol.printStats( std::cout );
    std::cout<<"*******************"<<std::endl;

    aol.extractOverLine( 1346.58597583396, 1327.36049859144, 1362.49909026951, 1313.73856416995 );
    aol.saveLine2VTK( prefix + "_izvor" );
    aol.printIntegrals( std::cout );
    aol.printStats( std::cout );
    std::cout<<"*******************"<<std::endl;
    
    const double tx = -5456491.999777; //x translation of coordinate system
    const double ty = -5019989.003335; //y translation of coordinate system
    
    aol.extractOverLine( 5457249.66+tx, 5019986.53+ty, 5457222.72+tx, 5019978.27+ty );
    aol.saveLine2VTK( prefix + "_Zelj_most" );
    aol.printIntegrals( std::cout );
    aol.printStats( std::cout );
    std::cout<<"*******************"<<std::endl;
    
    aol.extractOverLine( 5457209.51+tx, 5020081.34+ty, 5457182.69+tx, 5020076.23+ty );
    aol.saveLine2VTK( prefix + "_cesta D404" );
    aol.printIntegrals( std::cout );
    aol.printStats( std::cout );
    std::cout<<"*******************"<<std::endl;
    
    aol.extractOverLine( 5457148.02+tx, 5020256.28+ty, 5457120.2+tx, 5020248.65+ty );
    aol.saveLine2VTK( prefix + "_cestovni most_01" );
    aol.printIntegrals( std::cout );
    aol.printStats( std::cout );
    std::cout<<"*******************"<<std::endl;
    
    aol.extractOverLine( 5457148.83+tx, 5020399.5+ty, 5457120.91+tx, 5020412.77+ty );
    aol.saveLine2VTK( prefix + "_cestovni most_02" );
    aol.printIntegrals( std::cout );
    aol.printStats( std::cout );
    std::cout<<"*******************"<<std::endl;
    
    aol.extractOverLine( 5457148.61+tx, 5020478.77+ty, 5457124.58+tx, 5020478.83+ty );
    aol.saveLine2VTK( prefix + "_Kontinental" );
    aol.printIntegrals( std::cout );
    aol.printStats( std::cout );
    std::cout<<"*******************"<<std::endl;

    aol.extractOverLine( 5457147.64+tx, 5020503.79+ty, 5457127.33+tx, 5020504.57+ty );
    aol.saveLine2VTK( prefix + "_cestovni most_03" );
    aol.printIntegrals( std::cout );
    aol.printStats( std::cout );
    std::cout<<"*******************"<<std::endl;
    
    aol.extractOverLine( 5457160.21+tx, 5020580.84+ty, 5457144.18+tx, 5020592.79+ty );
    aol.saveLine2VTK( prefix + "_cestovni most_04" );
    aol.printIntegrals( std::cout );
    aol.printStats( std::cout );
    std::cout<<"*******************"<<std::endl;
    
    aol.extractOverLine( 5457396.21+tx, 5021171.03+ty, 5457378.22+tx, 5021170.75+ty );
    aol.saveLine2VTK( prefix + "_HE_Rijeka" );
    aol.printIntegrals( std::cout );
    aol.printStats( std::cout );
    std::cout<<"*******************"<<std::endl;
    
    aol.extractOverLine( 5457652.63+tx, 5021231.29+ty, 5457664.59+tx, 5021245.76+ty );
    aol.saveLine2VTK( prefix + "_Hartera" );
    aol.printIntegrals( std::cout );
    aol.printStats( std::cout );
    std::cout<<"*******************"<<std::endl;

    return 0;

}
