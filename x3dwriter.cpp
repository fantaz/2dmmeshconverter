/*=========================================================================

  Program:   Visualization Toolkit
  Module:    X3DTest.cxx

  Copyright (c) Ken Martin, Will Schroeder, Bill Lorensen
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/Copyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
#include "vtkActor.h"
#include "vtkConeSource.h"
#include "vtkDebugLeaks.h"
#include "vtkGlyph3D.h"
#include "vtkPolyData.h"
#include "vtkPolyDataMapper.h"
//#include "vtkRegressionTestImage.h"
#include "vtkRenderWindow.h"
#include "vtkRenderWindowInteractor.h"
#include "vtkRenderer.h"
#include "vtkRenderer.h"
#include "vtkSphereSource.h"

#include "vtkX3DExporter.h"
#include <vtkSTLWriter.h>
#include "vtkXMLUnstructuredGridReader.h"
#include "vtkXMLUnstructuredGridWriter.h"
#include "vtkXMLStructuredDataReader.h"
#include "vtkXMLStructuredGridReader.h"
#include "vtkUnstructuredGridReader.h"
#include "vtkUnstructuredGridWriter.h"
#include "vtkUnstructuredGrid.h"
#include "vtkSmartPointer.h"
#include "vtkPointData.h"
#include <boost/config/posix_features.hpp>
#include "pathconfig.h"
#include <vtkDataSetSurfaceFilter.h>
#include <vtkDecimatePro.h>
#include <vtkTriangleFilter.h>
#include <vtkGeometryFilter.h>
template<class writerType>
void writeGeometry ( vtkDataObject* geom, const std::string fname )
{
    std::cout<<"writing "<<fname<<"....";
    writerType* writer =  writerType::New();
    writer->SetFileName ( fname.c_str() );
#if VTK_MAJOR_VERSION <= 5
    writer->SetInput ( geom );
#else
    writer->SetInputData ( geom );
#endif
    writer->SetDataModeToAscii();
    writer->Write();
    writer->Delete();
    std::cout<<"done."<<std::endl;
}

int main( )
{
    std::string fname = filePathBuilder::dataPath() + "simres-10000.00_2.vtk";
    //std::string fname = filePathBuilder::dataPath() + "nesto2.vtk";
    vtkSmartPointer<vtkUnstructuredGridReader> reader = vtkSmartPointer<vtkUnstructuredGridReader>::New();
    reader->SetFileName ( fname.c_str() );
    reader->Update();

    vtkPointData *pd = reader->GetOutput()->GetPointData();

    if ( pd )
    {
        std::cout << " contains point data with "
                  << pd->GetNumberOfArrays()
                  << " arrays." << std::endl;

        for ( int i = 0; i < pd->GetNumberOfArrays(); i++ )
        {
            std::cout << "\tArray " << i
                      << " is named "
                      << ( pd->GetArrayName ( i ) ? pd->GetArrayName ( i ) : "NULL" )
                      << std::endl;
        }
    }

    int br = reader->GetOutput()->GetPointData()->SetActiveAttribute ( "H",vtkDataSetAttributes::SCALARS );
    std::cout<<"br = "<<br<<std::endl;
    vtkDataArray* Harr = reader->GetOutput()->GetPointData()->GetArray ( br );
    br = reader->GetOutput()->GetPointData()->SetActiveAttribute ( "P",vtkDataSetAttributes::SCALARS );
    vtkDataArray* Parr = reader->GetOutput()->GetPointData()->GetArray ( br );
    br = reader->GetOutput()->GetPointData()->SetActiveAttribute ( "Zb",vtkDataSetAttributes::SCALARS );
    vtkDataArray* Zbarr = reader->GetOutput()->GetPointData()->GetArray ( br );

    vtkDataArray* arrcoords = reader->GetOutput()->GetPoints()->GetData();
    double pcoords[3];

    for ( int i=0; i< arrcoords->GetNumberOfTuples(); ++i )
    {
        arrcoords->GetTuple ( i,pcoords );
        /*
        if ( Parr->GetTuple1 ( i ) >0.03 )
        { pcoords[2] = Harr->GetTuple1 ( i ); }
        else
        {pcoords[2] = -10.;}
        */
        pcoords[2] = Zbarr->GetTuple1 ( i );
        arrcoords->SetTuple ( i,pcoords );
    }

    writeGeometry<vtkXMLUnstructuredGridWriter> ( reader->GetOutput(), filePathBuilder::dataPath() + "promjena.vtu" );

    vtkSmartPointer<vtkDataSetSurfaceFilter> surfaceFilter =  vtkSmartPointer<vtkDataSetSurfaceFilter>::New();
    surfaceFilter->SetInputData ( reader->GetOutput() );
    surfaceFilter->Update();
    vtkPolyData* polydata = surfaceFilter->GetOutput();
    
    
    vtkSmartPointer<vtkTriangleFilter> tri = vtkSmartPointer<vtkTriangleFilter>::New();
    //tri->SetInputConnection(reader->GetOutputPort());
    tri->SetInputData(polydata);
    tri->Update();
    vtkSmartPointer<vtkDecimatePro> deci = vtkSmartPointer<vtkDecimatePro>::New();
    deci->SetInputConnection(tri->GetOutputPort());
    deci->Update();
    
    //std::string stlout = filePathBuilder::dataPath() + "promjena_deci.stl";
    std::string stlout = filePathBuilder::dataPath() + "teren.stl";
    vtkSmartPointer<vtkSTLWriter> exporter= vtkSmartPointer<vtkSTLWriter>::New();
    //exporter->SetInputData ( polydata );
    exporter->SetInputConnection(deci->GetOutputPort());
    exporter->SetFileName ( stlout.c_str() );
    exporter->Update();
    exporter->Write();
    exporter->Print ( std::cout );
    /*
    writeGeometry<vtkXMLUnstructuredGridWriter>(reader->GetOutput(), filePathBuilder::dataPath() + "nesto.vtu");
    {
        vtkSmartPointer<vtkXMLUnstructuredGridReader> ureader = vtkSmartPointer<vtkXMLUnstructuredGridReader>::New();
        std::string fnr = filePathBuilder::dataPath() + "nesto2.vtu";
        ureader->SetFileName(fnr.c_str());
        ureader->Update();

        std::string fn = filePathBuilder::dataPath() + "nesto2.vtk";
        vtkSmartPointer<vtkUnstructuredGridWriter> writer = vtkSmartPointer<vtkUnstructuredGridWriter>::New();
        writer->SetInputData(ureader->GetOutput());
        writer->SetFileName(fn.c_str());
        writer->SetFileTypeToASCII();
        writer->Update();

    }*/
    //reader->GetOutput()->Print ( std::cout );
    return 0;
    /*
    vtkSphereSource *sphere = vtkSphereSource::New();
    sphere->SetThetaResolution ( 8 );
    sphere->SetPhiResolution ( 8 );
    vtkPolyDataMapper *sphereMapper = vtkPolyDataMapper::New();
    sphereMapper->SetInputConnection ( sphere->GetOutputPort() );
    vtkActor *sphereActor = vtkActor::New();
    sphereActor->SetMapper ( sphereMapper );

    vtkConeSource *cone = vtkConeSource::New();
    cone->SetResolution ( 6 );

    vtkGlyph3D *glyph = vtkGlyph3D::New();
    glyph->SetInputConnection ( sphere->GetOutputPort() );
    glyph->SetSourceConnection ( cone->GetOutputPort() );
    glyph->SetVectorModeToUseNormal();
    glyph->SetScaleModeToScaleByVector();
    glyph->SetScaleFactor ( 0.25 );

    vtkPolyDataMapper *spikeMapper = vtkPolyDataMapper::New();
    spikeMapper->SetInputConnection ( glyph->GetOutputPort() );

    vtkActor *spikeActor = vtkActor::New();
    spikeActor->SetMapper ( spikeMapper );

    vtkRenderer *renderer = vtkRenderer::New();
    vtkRenderWindow *renWin = vtkRenderWindow::New();
    renWin->AddRenderer ( renderer );
    vtkRenderWindowInteractor *iren = vtkRenderWindowInteractor::New();
    iren->SetRenderWindow ( renWin );

    renderer->AddActor ( sphereActor );
    renderer->AddActor ( spikeActor );
    renderer->SetBackground ( 1,1,1 );
    renWin->SetSize ( 300,300 );

    // interact with data
    renWin->Render();

    vtkX3DExporter *exporter = vtkX3DExporter::New();
    exporter->SetInput ( renWin );
    exporter->SetFileName ( "testX3DExporter.x3d" );
    exporter->Update();
    exporter->Write();
    exporter->Print ( std::cout );

    iren->Initialize();
    iren->Start();

    // Clean up
    exporter->Delete();
    renderer->Delete();
    renWin->Delete();
    iren->Delete();
    sphere->Delete();
    sphereMapper->Delete();
    sphereActor->Delete();
    cone->Delete();
    glyph->Delete();
    spikeMapper->Delete();
    spikeActor->Delete();
    */
    return 0;
}

