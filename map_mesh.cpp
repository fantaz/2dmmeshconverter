#include <iostream>
#include <cassert>
#include <vtkUnstructuredGridReader.h>
#include <vtkXMLUnstructuredGridWriter.h>
#include <vtkXMLUnstructuredGridReader.h>
#include <vtkXMLPolyDataWriter.h>
#include <vtkXMLPolyDataReader.h>
#include <vtkUnstructuredGrid.h>
#include <vtkSmartPointer.h>
#include <vtkProbeFilter.h>
#include <vtkIdTypeArray.h>
#include <vtkSimplePointsWriter.h>

#include <vtkWarpScalar.h>

#include <vtkDelaunay2D.h>
#include "pathconfig.h"
#include <vtkPolyData.h>
#include <vtkPointData.h>

#include <vtkDataSetSurfaceFilter.h>
#include <vtkTriangleFilter.h>
#include <vtkSTLWriter.h>

#include <vtkTransform.h>
#include <vtkGeometryFilter.h>
#include <vtkTransformPolyDataFilter.h>
#include <boost/program_options.hpp>
#include <boost/filesystem.hpp>



template<class writerType>
void writeGeometry( vtkDataObject *geom, const std::string fname )
{

    writerType *writer =  writerType::New();
    std::string fullname = fname + "." + writer->GetDefaultFileExtension();
    std::cout << "writing " << fullname << "....";
    writer->SetFileName( fullname.c_str() );
#if VTK_MAJOR_VERSION <= 5
    writer->SetInput( geom );
#else
    writer->SetInputData( geom );
#endif
    writer->SetDataModeToBinary();
    writer->Write();
    writer->Delete();
    std::cout << "done." << std::endl;
}

vtkSmartPointer<vtkPointSet> readPointSet( const boost::filesystem::path &pth )
{

    if ( pth.extension().string() == ".vtk" )
    {
        vtkSmartPointer<vtkUnstructuredGridReader> source_reader = vtkSmartPointer<vtkUnstructuredGridReader>::New();
        //input_reader->SetFileName( ( filePathBuilder::dataPath() + "post_gerris_output.vtk" ).c_str() );
        source_reader->SetFileName( pth.string().c_str() );
        source_reader->Update();
        source_reader->GetOutput();
        return source_reader->GetOutput();

    }
    else if ( pth.extension().string() == ".vtu" )
    {
        vtkSmartPointer<vtkXMLUnstructuredGridReader> source_reader = vtkSmartPointer<vtkXMLUnstructuredGridReader>::New();
        source_reader->SetFileName( pth.string().c_str() );
        source_reader->Update();
        return source_reader->GetOutput();
    }

    //else //pth.extension().string() == ".vtp"

    vtkSmartPointer<vtkXMLPolyDataReader> source_reader = vtkSmartPointer<vtkXMLPolyDataReader>::New();
    source_reader->SetFileName( pth.string().c_str() );
    source_reader->Update();
    return source_reader->GetOutput();
    //else  //sfpath.extension().string() == ".vtu"



}
int main( int argc, char *argv[] )
{
    namespace po = boost::program_options;

    boost::filesystem::path ifpath; // input filename
    boost::filesystem::path sfpath; // source filename
    boost::filesystem::path ofpath; // output filename
    bool vtk_out {false};
    bool zb_out {false};
    bool xyz_out {false};
    std::string attribute {"H"};
    po::options_description desc( "Allowed options" );
    desc.add_options()
    ( "help", "produce this help message\n Creates STL or xyz on interpolated mesh." )
    ( "input", po::value<std::string>(), "full path to vtk mesh that needs to be interpolated" )
    ( "source", po::value<std::string>(), "full path to vtk mesh that holds scalars which will be mapped on input mesh" )
    ( "mesh-out", po::value<std::string>(), "full path to interpolated stl or xyz mesh" )
    ( "vtk-out", po::value<bool>( &vtk_out )->default_value( false ), "yes to save additional vtk output." )
    //( "zb-out", po::value<bool>( &zb_out )->default_value( false ), "yes to save Zb, H otherwise." )
    ( "xyz-out", po::value<bool>( &xyz_out )->default_value( false ), "yes to scalars to xyz, otherwise output to stl." )
    ( "attribute", po::value<std::string>( &attribute )->default_value( "H" ), "attribute name, default is H." )
    ;
    po::variables_map vm;
    po::store( po::parse_command_line( argc, argv, desc ), vm );

    if ( vm.count( "help" ) )
    {
        cout << desc << "\n";
        return 0;
    }

    if ( vm.count( "input" ) )
    {
        boost::filesystem::path tmpf( vm["input"].as<std::string>() );

        if ( boost::filesystem::exists( tmpf ) && ( tmpf.extension().string() == ".vtk"  || tmpf.extension().string() == ".vtu" || tmpf.extension().string() == ".vtp") )
        {
            ifpath = tmpf;
        }
        else {std::cerr << "not vtk file or input file doesn't exist" << std::endl; return 0;}
    }
    else
    { std::cout << "no input file given, aborting" << std::endl; return 0; }

    if ( vm.count( "source" ) )
    {
        boost::filesystem::path tmpf( vm["source"].as<std::string>() );

        if ( boost::filesystem::exists( tmpf ) && ( tmpf.extension().string() == ".vtk"  || tmpf.extension().string() == ".vtu" ) )
        {
            sfpath = tmpf;
        }
        else {std::cerr << "not vtk file or source file doesn't exist" << std::endl; return 0;}
    }
    else
    { std::cout << "no source file given, aborting" << std::endl; return 0; }

    if ( vm.count( "mesh-out" ) )
    {
        boost::filesystem::path tmpf( vm["mesh-out"].as<std::string>() );

        if ( boost::filesystem::exists( tmpf.parent_path() ) )
        {
            ofpath = tmpf;
        }
        else {std::cerr << " directory path to mesh file does not exist" << std::endl; return 0;}
    }
    else
    { std::cout << "no stl or xyz file given, aborting" << std::endl; return 0; }

    if ( vm.count( "vtk-out" ) )
    {   vtk_out = vm["vtk-out"].as<bool>(); }

    //if ( vm.count( "zb-out" ) )
    //{   zb_out = vm["zb-out"].as<bool>(); }

    if ( vm.count( "xyz-out" ) )
    {   xyz_out = vm["xyz-out"].as<bool>(); }
    
    if ( vm.count( "attribute" ) )
    {   attribute = vm["attribute"].as<std::string>(); }

    std::cout << " ofpath: " << ofpath.parent_path().string() << std::endl;
    std::cout << " ifpath: " << ifpath.parent_path().string() << std::endl;
    std::cout << " sfpath: " << sfpath.parent_path().string() << std::endl;
    std::cout << " source stem: " << sfpath.stem().string() << std::endl;
    std::cout << " input stem: " << ifpath.stem().string() << std::endl;
    std::cout << " out stem: " << ofpath.stem().string() << std::endl;
    std::cout << " vtk_out: " << vtk_out << std::endl;
    //std::cout << " zb_out: " << zb_out << std::endl;
    std::cout << " xyz_out: " << xyz_out << std::endl;
    std::cout << " attribute name: " << attribute << std::endl;

    // input
    vtkSmartPointer<vtkDelaunay2D> source_mesher = vtkSmartPointer<vtkDelaunay2D>::New();
    //source_mesher->SetInputConnection( source_reader->GetOutputPort() );
    source_mesher->SetInputData( readPointSet( sfpath ) );
    source_mesher->Update();

    
    
    vtkSmartPointer<vtkGeometryFilter> source_filter = vtkSmartPointer<vtkGeometryFilter>::New();
    source_filter->SetInputConnection(source_mesher->GetOutputPort());
    source_filter->Update();
    
    vtkSmartPointer<vtkGeometryFilter> input_filter = vtkSmartPointer<vtkGeometryFilter>::New();
    input_filter->SetInputData(readPointSet(ifpath));
    input_filter->Update();
    
    vtkSmartPointer<vtkTransform> flattener = vtkSmartPointer<vtkTransform>::New();
    flattener->Scale( 1.0, 1.0, 0.0 );

    vtkSmartPointer<vtkTransformPolyDataFilter> input_flat = vtkSmartPointer<vtkTransformPolyDataFilter>::New();
    input_flat->SetInputConnection( input_filter->GetOutputPort() );
    input_flat->SetTransform( flattener );
    input_flat->Update();
    
    vtkSmartPointer<vtkTransformPolyDataFilter> source_flat = vtkSmartPointer<vtkTransformPolyDataFilter>::New();
    source_flat->SetInputConnection( source_filter->GetOutputPort() );
    source_flat->SetTransform( flattener );
    source_flat->Update();
    

    vtkSmartPointer<vtkProbeFilter> probe = vtkSmartPointer<vtkProbeFilter>::New();
    //probe->SetInputData( readPointSet( ifpath ) );
    probe->SetInputConnection(input_flat->GetOutputPort());
    probe->SetSourceConnection( source_flat->GetOutputPort() );
    //probe->SetSourceConnection( source_mesher->GetOutputPort() );
    probe->Update();

    std::cout << "number of points: " << probe->GetOutput()->GetNumberOfPoints() << "\n valid points: " << probe->GetValidPoints()->GetNumberOfTuples() << std::endl;
    assert( probe->GetOutput()->GetNumberOfPoints() == probe->GetValidPoints()->GetNumberOfTuples() );


    /*vtkSmartPointer<vtkXMLUnstructuredGridWriter> blenter_out = vtkSmartPointer<vtkXMLUnstructuredGridWriter>::New();
    //blenter_out->SetFileName( ( filePathBuilder::resultsPath() + "post_blender_output.vtu" ).c_str() );
    blenter_out->SetInputConnection(  );
    blenter_out->SetDataModeToBinary();
    blenter_out->Update();
    */

    /*if ( zb_out )
    {probe->GetOutput()->GetPointData()->SetActiveAttribute( "Zb", vtkDataSetAttributes::SCALARS );}
    else
    {probe->GetOutput()->GetPointData()->SetActiveAttribute( "H", vtkDataSetAttributes::SCALARS );}*/
    probe->GetOutput()->GetPointData()->SetActiveAttribute( attribute.c_str(), vtkDataSetAttributes::SCALARS );

    vtkSmartPointer<vtkWarpScalar> warp = vtkSmartPointer<vtkWarpScalar>::New();
    warp->SetInputConnection( probe->GetOutputPort() );
    warp->SetScaleFactor( 1 );
    warp->SetNormal( 0, 0, 1 );
    warp->UseNormalOn();
    warp->Update();



    vtkSmartPointer<vtkDataSetSurfaceFilter> u2poly = vtkSmartPointer<vtkDataSetSurfaceFilter>::New();
    u2poly->SetInputConnection( warp->GetOutputPort() );
    u2poly->Update();

    vtkSmartPointer<vtkTriangleFilter> triangleFilter = vtkSmartPointer<vtkTriangleFilter>::New();
    triangleFilter->SetInputConnection( u2poly->GetOutputPort() );
    triangleFilter->Update();

    if ( !xyz_out )
    {
        vtkSmartPointer<vtkSTLWriter> stlWriter = vtkSmartPointer<vtkSTLWriter>::New();
        //stlWriter->SetFileName( std::string(filePathBuilder::resultsPath() + "warp_blender_output.stl").c_str() );
        stlWriter->SetFileName( ofpath.string().c_str() );
        stlWriter->SetInputConnection( triangleFilter->GetOutputPort() );
        stlWriter->Write();
    }
    else
    {
        vtkSmartPointer<vtkSimplePointsWriter> xyzWriter = vtkSmartPointer<vtkSimplePointsWriter>::New();
        xyzWriter->SetFileName( ofpath.string().c_str() );
        xyzWriter->SetInputConnection( triangleFilter->GetOutputPort() );
        xyzWriter->Write();
    }

    if ( vtk_out )
    {
        writeGeometry<vtkXMLUnstructuredGridWriter>( warp->GetOutput(),
                ofpath.parent_path().string() + "/" + ofpath.stem().string() );

        writeGeometry<vtkXMLUnstructuredGridWriter>( probe->GetOutput(),
                ofpath.parent_path().string() + "/" + sfpath.stem().string() );
    }

    return 0;
}
