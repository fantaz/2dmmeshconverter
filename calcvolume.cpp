#include <vtkSmartPointer.h>
#include <vtkPoints.h>
#include <vtkPolyData.h>
#include <vtkDelaunay2D.h>
#include <vtkXMLPolyDataWriter.h>
#include <iostream>
#include <vtkPlane.h>
#include <vtkPlaneSource.h>
#include <vtkXMLPolyDataReader.h>
#include "2dmReader.h"
#include <vtkSimplePointsReader.h>
#include <vtkDelaunay3D.h>
#include <vtkUnstructuredGrid.h>
#include <vtkTetra.h>
#include <boost/program_options.hpp>
#include <boost/filesystem.hpp>

template<class writerType>
void writeGeometry( vtkDataObject *geom, const std::string fname )
{
    std::cout << "writing " << fname << "....";
    writerType *writer =  writerType::New();
    writer->SetFileName( fname.c_str() );
#if VTK_MAJOR_VERSION <= 5
    writer->SetInput( geom );
#else
    writer->SetInputData( geom );
#endif
    writer->SetDataModeToBinary();
    //writer->SetDataModeToAscii();
    writer->Write();
    writer->Delete();
    std::cout << "done." << std::endl;
}

// void calcVolume( vtkPolyData *poly )
// {
// //     vtkSmartPointer<vtkMassProperties> vol = vtkSmartPointer<vtkMassProperties>::New();
// //     vol->SetInputData( poly );
// //     vol->Update();
//
// //     int prec = std::numeric_limits<double>::digits10 + 2; // generally 17
// //     int exponent_digits = std::log10( std::numeric_limits<double>::max_exponent10 ) + 1; // generally 3
// //     int exponent_sign   = 1; // 1.e-123
// //     int exponent_symbol = 1; // 'e' 'E'
// //     int digits_sign = 1;
// //     int digits_dot = 1; // 1.2
// //
// //     int division_extra_space = 1;
// //     int width = prec + exponent_digits + digits_sign + exponent_sign + digits_dot + exponent_symbol + division_extra_space;
//
//
//     /*std::cout  << "volume: " << std::setprecision( prec ) << std::setw( width ) << vol->GetVolume() << std::endl;
//     std::cout << "projected volume: " << std::setprecision( prec ) << std::setw( width ) << vol->GetVolumeProjected() << std::endl;
//     std::cout << "volume x : " << std::setprecision( prec ) << std::setw( width ) << vol->GetVolumeX() << std::endl;
//     std::cout << "volume y : " << std::setprecision( prec ) << std::setw( width ) << vol->GetVolumeY() << std::endl;
//     std::cout << "volume z : " << std::setprecision( prec ) << std::setw( width ) << vol->GetVolumeZ() << std::endl;
//
//     std::cout << "diff: " << ( vol->GetVolume() - vol->GetVolumeProjected() ) * 10000 << std::endl;
//      */
// }

vtkSmartPointer<vtkPolyData> prepareXYZFile( std::string fname )
{
    vtkSmartPointer<vtkSimplePointsReader> reader_xyz = vtkSmartPointer<vtkSimplePointsReader>::New();
    reader_xyz->SetFileName( fname.c_str() );
    reader_xyz->Update();
    vtkSmartPointer<vtkDelaunay2D> delaunay = vtkSmartPointer<vtkDelaunay2D>::New();
    delaunay->SetInputData( reader_xyz->GetOutput() );
    delaunay->Update();
    return delaunay->GetOutput();
}
int main( int argc, char *argv[] )
{

    boost::filesystem::path ifpath; // input filename
    double cut_elevation {10.};
    bool vtk_out {false};
    bool verbose {false};

    namespace po = boost::program_options;
    po::options_description desc( "Allowed options" );
    desc.add_options()
    ( "help", "produce this help message\n Calculates volume to z plane." )
    ( "input", po::value<std::string>(), "full path to mesh for which the volume needs to be calculated" )
    ( "elevation", po::value < double>( &cut_elevation ), "elevation" )
    ( "vtk-out", po::value<bool>( &vtk_out )->default_value( false ), "yes to save additional vtk output." )
    ( "verbose", po::value<bool>( &verbose )->default_value( false ), "yes to show additional messages." )
    ;

    po::variables_map vm;
    po::store( po::parse_command_line( argc, argv, desc ), vm );

    if ( vm.count( "help" ) )
    {
        cout << desc << "\n";
        return 0;
    }

    if ( vm.count( "input" ) )
    {
        boost::filesystem::path tmpf( vm["input"].as<std::string>() );

        if ( boost::filesystem::exists( tmpf ) && ( tmpf.extension().string() == ".vtp" ||
                tmpf.extension().string() == ".2dm" || tmpf.extension().string() == ".xyz" )
           )
        {
            ifpath = tmpf;
        }
        else {std::cerr << "not vtp, xyz, 2dm file or input file doesn't exist" << std::endl; return 0;}
    }
    else
    { std::cout << "no input file given, aborting" << std::endl; return 0; }

    if ( vm.count( "vtk-out" ) )
    {   vtk_out = vm["vtk-out"].as<bool>(); }

    if ( vm.count( "verbose" ) )
    {   verbose = vm["verbose"].as<bool>(); }

    if ( vm.count( "elevation" ) )
    {   cut_elevation = vm["elevation"].as<double>(); }


    if ( verbose )
    {
        std::cout << " ifpath: " << ifpath.parent_path().string() << std::endl;
        std::cout << " elevation: " << cut_elevation << std::endl;
        std::cout << " vtk_out: " << vtk_out << std::endl;

        if ( vtk_out )
        {std::cout << " vtkout path: " << ifpath.parent_path().string() << std::endl;}
    }

    vtkSmartPointer<vtkPolyData> mesh2d;

    if ( ifpath.extension().string() == ".xyz" )
    {
        mesh2d = prepareXYZFile( ifpath.c_str() );
    }

    double origin[3] = {0.0, 0.0, cut_elevation};
    double normal[3] = {0.0, 0.0, 1.0};

    vtkSmartPointer<vtkPlane> plane =  vtkSmartPointer<vtkPlane>::New();
    plane->SetOrigin( origin );
    plane->SetNormal( normal );
    double projected[3];

    double terrain_volume {0.};
    vtkSmartPointer<vtkPoints> projectedpts = vtkSmartPointer<vtkPoints>::New();
    projectedpts->SetNumberOfPoints( 6 );
    vtkSmartPointer<vtkPolyData>poly = vtkSmartPointer<vtkPolyData>::New();
    vtkSmartPointer<vtkDelaunay3D> delaunay3D = vtkSmartPointer<vtkDelaunay3D>::New();

    for ( int i = 0; i < mesh2d->GetNumberOfPolys() ; ++i )
    {
        double p[3];

        for ( int j = 0; j < mesh2d->GetCell( i )->GetNumberOfPoints(); ++j )
        {
            assert( mesh2d->GetCell( i )->GetNumberOfPoints() == 3 );
            mesh2d->GetCell( i )->GetPoints()->GetPoint( j, p );
            plane->ProjectPoint( p, origin, normal, projected );
            projectedpts->SetPoint( j * 2, p );
            projectedpts->SetPoint( j * 2 + 1, projected );
        }

        poly->SetPoints( projectedpts );

        delaunay3D->SetInputData( poly );
        delaunay3D->Update();

        double cell_volume {0.};

        for ( int i = 0; i < delaunay3D->GetOutput()->GetNumberOfCells(); ++i )
        {
            vtkTetra *t = static_cast<vtkTetra *>( delaunay3D->GetOutput()->GetCell( i ) );

            if ( t )
            {
                double p0[3], p1[3], p2[3], p3[3];
                t->GetPoints()->GetPoint( 0, p0 );
                t->GetPoints()->GetPoint( 1, p1 );
                t->GetPoints()->GetPoint( 2, p2 );
                t->GetPoints()->GetPoint( 3, p3 );
                cell_volume += vtkTetra::ComputeVolume( p0, p1, p2, p3 );

            }
        }

        //std::cout << "cell " << i << "\tvol = " << cell_volume << std::endl;
        terrain_volume += cell_volume;
    }

    //std::cout << "terrain_volume  = " << terrain_volume << std::endl;
    int prec = std::numeric_limits<double>::digits10 + 2; // generally 17
    std::cout << ifpath.string() << " : " << 
    std::setprecision( prec )<<terrain_volume << std::endl;


    /*
    vtkSmartPointer<vtkPoints> points = vtkSmartPointer<vtkPoints>::New();
    points->InsertNextPoint( 0, 0, 0 );
    points->InsertNextPoint( 1, 0, 0 );
    points->InsertNextPoint( 1, 1, 0 );
    points->InsertNextPoint( 0, 2, 0 );
    vtkSmartPointer<vtkPolyData> polydata = vtkSmartPointer<vtkPolyData>::New();
    polydata->SetPoints( points );

    vtkSmartPointer<vtkDelaunay2D> delaunay = vtkSmartPointer<vtkDelaunay2D>::New();
    delaunay->SetInputData( polydata );
    delaunay->Update();


    double origin[3] = {0.0, 0.0, 3.0};
    double normal[3] = {0.0, 0.0, 1.0};

    vtkSmartPointer<vtkPlane> plane =  vtkSmartPointer<vtkPlane>::New();
    plane->SetOrigin( origin );
    plane->SetNormal( normal );
    double projected[3];

    double terrain_volume {0.};
    vtkSmartPointer<vtkPoints> projectedpts = vtkSmartPointer<vtkPoints>::New();
    projectedpts->SetNumberOfPoints( 6 );
    vtkSmartPointer<vtkPolyData>poly = vtkSmartPointer<vtkPolyData>::New();
    vtkSmartPointer<vtkDelaunay3D> delaunay3D = vtkSmartPointer<vtkDelaunay3D>::New();

    for ( int i = 0; i < delaunay->GetOutput()->GetNumberOfPolys() ; ++i )
    {
        double p[3];

        for ( int j = 0; j < delaunay->GetOutput()->GetCell( i )->GetNumberOfPoints(); ++j )
        {
            assert( delaunay->GetOutput()->GetCell( i )->GetNumberOfPoints() == 3 );
            delaunay->GetOutput()->GetCell( i )->GetPoints()->GetPoint( j, p );
            plane->ProjectPoint( p, origin, normal, projected );
            projectedpts->SetPoint( j * 2, p );
            projectedpts->SetPoint( j * 2 + 1, projected );
        }

        poly->SetPoints( projectedpts );


        delaunay3D->SetInputData( poly );
        delaunay3D->Update();

        double cell_volume {0.};

        for ( int i = 0; i < delaunay3D->GetOutput()->GetNumberOfCells(); ++i )
        {
            vtkTetra *t = static_cast<vtkTetra *>( delaunay3D->GetOutput()->GetCell( i ) );

            if ( t )
            {
                double p0[3], p1[3], p2[3], p3[3];
                t->GetPoints()->GetPoint( 0, p0 );
                t->GetPoints()->GetPoint( 1, p1 );
                t->GetPoints()->GetPoint( 2, p2 );
                t->GetPoints()->GetPoint( 3, p3 );
                cell_volume += vtkTetra::ComputeVolume( p0, p1, p2, p3 );

            }
        }

        std::cout << "cell " << i << "\tvol = " << cell_volume << std::endl;
        terrain_volume += cell_volume;
    }

    std::cout << "terrain_volume  = " << terrain_volume << std::endl;
    */


    return 0;
}


