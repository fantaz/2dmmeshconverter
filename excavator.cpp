#include "2dmReader.h"
#include <vtkXMLPolyDataWriter.h>
#include <vtkPolyData.h>
#include <vtkTransform.h>
#include <vtkTransformPolyDataFilter.h>

#include <iostream>
#include <algorithm>
#include <boost/program_options.hpp>
#include <boost/filesystem.hpp>
#include <boost/thread.hpp>

template<class writerType>
void writeGeometry( vtkDataObject *geom, const std::string fname )
{
    std::cout << "writing " << fname << "....";
    writerType *writer =  writerType::New();
    writer->SetFileName( fname.c_str() );
#if VTK_MAJOR_VERSION <= 5
    writer->SetInput( geom );
#else
    writer->SetInputData( geom );
#endif
    writer->SetDataModeToBinary();
    //writer->SetDataModeToAscii();
    writer->Write();
    writer->Delete();
    std::cout << "done." << std::endl;
}


class Excavator
{
private:
    std::vector<vtkPolygon *> flat_polys;
    vtkSmartPointer<vtkTransformPolyDataFilter> flat_terrain;
    vtkDataArray *poly_arr;
    std::vector<double> elevations;

    const int numberOfThreads;
    // pomocne


    vtkPolyData *f_terrain;

public:
    Excavator( std::vector<vtkPolygon *> fps,
               vtkSmartPointer<vtkTransformPolyDataFilter> ft,
               vtkDataArray *orig_data, std::vector<double> elevs, const int nthreads ) :
        flat_polys( fps ), flat_terrain( ft ), poly_arr( orig_data ),
        elevations( elevs ), numberOfThreads( nthreads )
    {
        f_terrain = flat_terrain->GetOutput();
    }

    struct normal
    {
        normal( double _n[3] )
        {for ( int i = 0; i < 3; ++i ) {n[i] = _n[i];}}
        double n[3];
    };
    struct bound
    {
        bound( double _b[6] )
        {for ( int i = 0; i < 6; ++i ) {bounds[i] = _b[i];}}
        double bounds[6];
    };
    void doExcavate( const int &threadId )
    {
        int elements = poly_arr->GetNumberOfTuples();
        int sz = elements / numberOfThreads;
        int b = sz * threadId;
        int e = b + sz - 1;

        if ( threadId == numberOfThreads - 1 ) {e = elements - 1;}

        double n[3];
        double opt[3];
        double bounds[6];
        std::vector<normal> normals;
        std::vector<bound> bound;

        for ( size_t id = 0; id < flat_polys.size(); id++ )
        {
            vtkPolygon *polygon = flat_polys[id];
            polygon->ComputeNormal( polygon->GetPoints()->GetNumberOfPoints(),
                                    static_cast<double *>( polygon->GetPoints()->GetData()->GetVoidPointer( 0 ) ), n );
            normals.push_back( n );
            polygon->GetPoints()->GetBounds( bounds );
            bound.push_back(bounds);
        }

        for ( int ptid = b; ptid <= e; ++ptid )
        {
            double testIn[3];
            f_terrain->GetPoints()->GetPoint( ptid, testIn );
            poly_arr->GetTuple( ptid, opt );

            for ( size_t id = 0; id < flat_polys.size(); id++ )
            {
                vtkPolygon *polygon = flat_polys[id];
                int inpolygon = vtkPolygon::PointInPolygon( testIn,
                                polygon->GetPoints()->GetNumberOfPoints(), static_cast<double *>(
                                    polygon->GetPoints()->GetData()->GetVoidPointer( 0 ) ), bound[id].bounds, normals[id].n );

                if ( inpolygon == 1 )
                {
                    opt[2] = elevations[id];
                    poly_arr->SetTuple( ptid, opt );
                }
            }
        }
    }
};
/**
 * \brief Postavlja na nulu mrezu i ulazne poligone
 */
std::tuple < vtkSmartPointer<vtkTransformPolyDataFilter>,
    std::vector<vtkSmartPointer<vtkTransformPolyDataFilter>> >
    flattenData( vtkSmartPointer<vtkPolyData> poly2dm,
                 std::vector<vtkSmartPointer<vtkPolyData>> nodestring_polys )
{
    // idemo na transformacije:
    vtkSmartPointer<vtkTransform> flattener = vtkSmartPointer<vtkTransform>::New();
    flattener->Scale( 1.0, 1.0, 0.0 );

    vtkSmartPointer<vtkTransformPolyDataFilter> poly_flat = vtkSmartPointer<vtkTransformPolyDataFilter>::New();
    poly_flat->SetInputData( poly2dm );
    poly_flat->SetTransform( flattener );
    poly_flat->Update();

    std::vector<vtkSmartPointer<vtkTransformPolyDataFilter>> nodestring_flat_polys;

    for ( size_t i = 0; i < nodestring_polys.size(); ++i )
    {
        vtkSmartPointer<vtkTransformPolyDataFilter> nodestring_flat = vtkSmartPointer<vtkTransformPolyDataFilter>::New();
        nodestring_flat->SetInputData( nodestring_polys[i] );
        nodestring_flat->SetTransform( flattener );
        nodestring_flat->Update();
        nodestring_flat_polys.push_back( nodestring_flat );
    }

    return std::make_tuple( poly_flat, nodestring_flat_polys );
}

int main( int ac, char *av[] )
{
    bool save_optional_files {true};
    boost::filesystem::path ifpath; // input filename
    int polygon_num {1}; // default is 0 polygons
    std::vector<double> elevations; // elevations to set in poylgons
    int numberOfThreads {1};

    namespace po = boost::program_options;
    namespace fs  = boost::filesystem;
    po::options_description desc( "Options" );
    desc.add_options()
    ( "help,h", "Usage: excavator --input <2dm input file> --polys <number of polygons> --elevations <list of terrain elevations, e.g. 21 39 etc.> --interm-save <yes to save intermediate files> --threads <number of threads>" )
    ( "input", po::value<std::string>(), "Input file" )
    ( "polys", po::value<int>( &polygon_num )->default_value( 1 ), "Number of polygons" )
    ( "elevations", po::value<std::vector<double>>( &elevations )->multitoken(),
      "Terrain elevations, should be no less than polygons-num, otherwise defaults to 0. " )
    ( "interm-save", po::value<bool>( &save_optional_files )->default_value( false ), "yes to save intermediate files" )
    ( "threads", po::value<int>( &numberOfThreads )->default_value( 1 ), "number of threads" );

    po::positional_options_description p;
    p.add( "elevations", -1 );
    po::variables_map vm;
    //po::store(po::parse_command_line(ac, av, desc), vm);
    //po::store( po::command_line_parser( ac, av ).options( desc ).positional( p ).run(), vm );
    po::store( po::command_line_parser( ac, av ).options( desc ).style(
                   po::command_line_style::unix_style ^ po::command_line_style::allow_short
               ).run(), vm );
    po::notify( vm );



    if ( vm.count( "help" ) )
    {
        cout << desc << "\n";
        return 0;
    }

    if ( vm.count( "input" ) )
    {
        boost::filesystem::path tmpf( vm["input"].as<std::string>() );

        if ( boost::filesystem::exists( tmpf ) && tmpf.extension().string() == ".2dm" )
        {
            ifpath = tmpf;
        }
        else {std::cerr << "not 2dm file or input file doesn't exist" << std::endl; return 0;}
    }
    else
    {
        std::cout << "no input file given, aborting" << std::endl;
        return 0;
    }

    if ( vm.count( "polys" ) )
    {
        polygon_num = vm["polys"].as<int>();
        std::cout << "number of polygons set to: " << polygon_num << std::endl;
    }
    else {std::cout << "number of polygons defaults to: " << polygon_num << std::endl;}

    if ( vm.count( "elevations" ) )
    {
        elevations = vm["elevations"].as<std::vector<double>>();
        std::cout << "elevations: ";

        for ( const auto & item : elevations )
        {
            std::cout << "\t" << item;
        }

        std::cout << std::endl;
    }
    else
    {
        elevations.resize( polygon_num );
        std::fill( elevations.begin(), elevations.end(), 0. );
        std::cout << "number of elevations equal to polygon_num and default to: 0" << std::endl;

    }

    // read 2dm
    Reader2dm reader( ifpath.stem().string(), ifpath.parent_path().string() + "/" );
    reader.createPolyData();

    // create polydata
    vtkSmartPointer<vtkPolyData> polydata = vtkSmartPointer<vtkPolyData>::New();
    polydata->SetPoints( reader.getPoints() );
    polydata->SetPolys( reader.getCells() );



    // idemo sada na nodestrings
    if ( !reader.hasNodestrings() || reader.getNumberOfNodestrings() < polygon_num )
    {
        std::cout << "has nodestrings: " << reader.hasNodestrings() << std::endl;
        std::cout << "nodestrings size: " << reader.getNumberOfNodestrings() << std::endl;
        std::cout << "polygon_num: " << polygon_num << std::endl;
        std::cout << "input file doesn't have nodestrings or their number is less than " <<
        "number specified by polygon_num.\n Aborting." << std::endl;
        return 0;
    }


    // extract polygons
    std::vector<vtkSmartPointer<vtkPolyData>> nodestring_polys = reader.getLastNPolygons( polygon_num );


    // flatten original vtp and polygons
    vtkSmartPointer<vtkTransformPolyDataFilter> poly_flat;
    std::vector<vtkSmartPointer<vtkTransformPolyDataFilter>> nodestring_flat_polys;
    std::tie( poly_flat, nodestring_flat_polys ) = flattenData( polydata, nodestring_polys );

    std::vector<vtkPolygon *> polygons;

    for ( auto & item : nodestring_flat_polys )
    {
        item->GetOutput()->GetPolys()->InitTraversal();
        vtkCell *cell = item->GetOutput()->GetCell( 0 );
        vtkPolygon *polygon = vtkPolygon::SafeDownCast( cell );

        if ( polygon ) {polygons.push_back( polygon );}
    }

    std::vector<Excavator> excavators;
    std::vector<boost::thread> threads;

    for ( int i = 0; i < numberOfThreads; ++i )
    {
        excavators.push_back( Excavator( polygons, poly_flat,
                                         polydata->GetPoints()->GetData(),
                                         elevations, numberOfThreads ) );
    }

    for (auto& item: excavators)
    {
        item.doExcavate(0);
    }
   /* for ( int i = 0; i < numberOfThreads; ++i )
    {
        threads.push_back( boost::thread( &Excavator::doExcavate, &excavators[i], i ) );
    }

    for ( int i = 0; i < numberOfThreads; ++i )
    {
        threads.at( i ).join();
    }*/

    // save iskopano:
    writeGeometry<vtkXMLPolyDataWriter> ( polydata, ifpath.parent_path().string() + "/" +
                                          ifpath.stem().string() + "_exscav.vtp" );

    if ( save_optional_files )
    {
        // optionally, save original vtp
        writeGeometry<vtkXMLPolyDataWriter> ( polydata,
                                              ifpath.parent_path().string() + "/" +
                                              ifpath.stem().string() + ".vtp" );

        // print nodestring to a file
        reader.writeNodestringsToFile( ifpath.parent_path().string() + "/" );

        // optionally save polygons
        for ( size_t i = 0; i < nodestring_polys.size(); ++i )
        {
            writeGeometry<vtkXMLPolyDataWriter> ( nodestring_polys[i],
                                                  ifpath.parent_path().string() + "/" +
                                                  ifpath.stem().string() + "_polygons_" +
                                                  std::to_string( i ) + ".vtp" );
        }

        // optionally, save original flat vtp
        writeGeometry<vtkXMLPolyDataWriter> ( poly_flat->GetOutput(),
                                              ifpath.parent_path().string() + "/" +
                                              ifpath.stem().string() + ".vtp" );

        // optionally save flatten polygons
        for ( size_t i = 0; i < nodestring_flat_polys.size(); ++i )
        {
            writeGeometry<vtkXMLPolyDataWriter> ( nodestring_flat_polys[i]->GetOutput(),
                                                  ifpath.parent_path().string() + "/" +
                                                  ifpath.stem().string() + "_polygons_flat_" +
                                                  std::to_string( i ) + ".vtp" );
        }
    }

    return 0;

}
