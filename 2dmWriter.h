#ifndef _2dmWriter_h
#define _2dmWriter_h

#include <vtkSmartPointer.h>
#include <vtkPolyData.h>
#include <fstream>
#include <vtkCellArray.h>

#include <string>
#include <fstream>
//#include <iostream>
class Writer2dm
{
public:
    Writer2dm(const std::string& out, vtkSmartPointer<vtkPolyData> input_file): out_file(out), pdata(input_file) 
    {}
    
    void write()
    {
        std::ofstream fout( out_file );
        fout << "MESH2D\nMESHNAME \"Mesh\"\n";

        //std::cout << pdata->GetPoints()->GetNumberOfPoints() << std::endl;
        //std::cout << pdata->GetPolys()->GetNumberOfCells() << std::endl;

        double pt[3];
    
        int prec = std::numeric_limits<double>::digits10 + 2; // generally 17
        int exponent_digits = std::log10( std::numeric_limits<double>::max_exponent10 ) + 1; // generally 3
        int exponent_sign   = 1; // 1.e-123
        int exponent_symbol = 1; // 'e' 'E'
        int digits_sign = 1;
        int digits_dot = 1;
        int division_extra_space = 1;
        int width = prec + exponent_digits + digits_sign + exponent_sign + digits_dot + exponent_symbol + division_extra_space;
        
        for ( int i = 0; i < pdata->GetPoints()->GetNumberOfPoints(); ++i )
        {
            pdata->GetPoints()->GetPoint( i, pt );
            fout << "ND " << i + 1 << " " << std::setprecision( prec ) << std::setw( width ) << pt[0];
            fout << " " << std::setprecision( prec ) << std::setw( width ) << pt[1];
            fout << " " << std::setprecision( prec ) << std::setw( width ) << pt[2] << std::endl;
        }

        pdata->GetPolys()->InitTraversal();
        vtkSmartPointer<vtkIdList> idList = vtkSmartPointer<vtkIdList>::New();

        int cellid=0;
        while ( pdata->GetPolys()->GetNextCell( idList ) )
        {
            //std::cout << "Line has " << idList->GetNumberOfIds() << " points." << std::endl;
            // output only triangles
            //if ( idList->GetNumberOfIds() == 3 )
            {
                fout << "E3T " << ++cellid << " ";
    
                for ( vtkIdType pointId = 0; pointId < idList->GetNumberOfIds(); pointId++ )
                {
                    fout << idList->GetId( pointId ) +1  << " ";
                }
                fout<<std::endl;
            }
        }
    }
private:
    std::string out_file;
    vtkSmartPointer<vtkPolyData> pdata;
};
#endif // _2dmWriter_h